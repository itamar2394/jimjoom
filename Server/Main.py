from JamsManager import JamsManager
from StreamsManager import StreamsManager
from RequestsHandler import RequestsHandler
from UsersManager import UsersManager
from DbManager import DbManager, WrongConnectionDetails
from TcpServer import TcpServer
from JimJoomServer import JimJoomServer
from NoInternetError import NoInternetError
from Logger import Logger


def main():
    """
    The main function of the server. Creates the needed objects and starts receiving Tcp requests using
    JimJoomServer.serve_forever()
    """
    while True:
        if NoInternetError.check_internet_connection():
            break
        else:
            input("No Internet Connection... press enter to try again")
    while True:
        try:
            tcp_server = TcpServer()
            break
        except OSError:
            input("Jimjoom server is already running in the machine...\npress enter to try again")
    while True:
        try:
            while True:
                with open("Db_config.txt", "r") as f:
                    try:
                        username, password = f.read().splitlines()
                        break
                    except ValueError:
                        input("Invalid configuration file format. Press enter to try again.")
            db_manager = DbManager(username, password, "jimjoom")
            break
        except WrongConnectionDetails:
            input("Wrong username or password. Or data base named 'jimjoom' does not exist. Press enter to try again.")
    db_manager.print_sweet_table("Users")
    users_manager = UsersManager(db_manager)
    streams_manager = StreamsManager(users_manager)
    jams_manager = JamsManager(users_manager, tcp_server, streams_manager)
    logger = Logger("jimjoom server logging").initialize("%(asctime)s     %(message)s", "jimjoom server logging.log")
    response_functions = {"LOG_IN": users_manager.log_in, "SIGN_UP": users_manager.create_account,
                          "ADD_TO_RANDOM_JAM_WAITING_LIST": jams_manager.add_to_random_jam_waiting_list,
                          "STOP_WAITING_FOR_RANDOM_JAM": jams_manager.stop_waiting_for_random_jam,
                          "CHECK_IF_USERNAME_EXISTS": users_manager.is_such_username,
                          "CREATE_JAM": jams_manager.create_private_jam,
                          "WAIT_FOR_INVITED_USERS": jams_manager.wait_for_invited_users,
                          "JOIN_PRIVATE_JAM": jams_manager.join_private_jam,
                          "START_JAM": jams_manager.start_jam,
                          "WAIT_FOR_JAM_UPDATES": jams_manager.wait_for_jam_updates,
                          "WAIT_FOR_HOST": jams_manager.wait_for_host,
                          "START_RECORDING": users_manager.start_recording,
                          "LEAVE_JAM": jams_manager.leave_jam,
                          "STOP_RECORDING": users_manager.stop_recording,
                          "RECEIVE_RECORDING": streams_manager.receive_recording,
                          "CLOSE_CLIENT": users_manager.close_client,
                          "DISCONNECT_FORM_JAM_THAT_DID_NOT_START_YET":
                              jams_manager.disconnect_from_jam_that_did_not_start_yet,
                          "CANCEL_JAM": jams_manager.cancel_jam,
                          "DELETE_RECORDING": streams_manager.delete_recording
                          }
    requests_handler = RequestsHandler(response_functions, logger, tcp_server, jams_manager)

    jimjoom = JimJoomServer(requests_handler, tcp_server)
    print("The server is up and running! Kick in the jams!")
    jimjoom.serve_forever()


if __name__ == "__main__":
    main()
