class User:

    """
    This class represent a user and contains all its data, in order to avoid repetitive and unnecessary retrieving from
    data base.
    """

    def __init__(self, username, location, instrument, genres, friends):
        """
        :param username: username of the user.
        :param location: name of the country the user is from.
        :param instrument: name of the instrument in which the user plays.
        :param genres: list of genres the user plays.
        :param friends: list of the user names of the user's friends. friends are users which the user had invited to a
        private jam in the past.
        """
        self.__username = username
        self.__location = location
        self.__instrument = instrument
        self.__genres = genres
        self.__friends = friends
        self.__port = None
        self.__jam = None
        self.__is_recording = False
        self.__recording_file_name = None
        self.__is_in_jam = False

    def get_username(self):
        return self.__username

    def get_location(self):
        return self.__location

    def get_instrument(self):
        return self.__instrument

    def get_genres(self):
        return self.__genres

    def get_friends(self):
        return self.__friends

    def get_port(self):
        return self.__port

    def get_recording_file_name(self):
        return self.__recording_file_name

    def set_port(self, port):
        self.__port = port

    def is_recording(self):
        return self.__is_recording

    def is_in_jam(self):
        return self.__is_in_jam

    def set_is_in_jam(self, value, jam=None):
        self.__is_in_jam = value
        if value:
            self.__jam = jam
        else:
            self.__jam = None

    def start_recording(self):
        """
        sets the is_recording file as True and initializing the recording file name.
        """
        self.__is_recording = True
        self.__recording_file_name = self.__username + self.__jam.jam_id

    def stop_recording(self):
        """
        sets the is_recording file as False.
        """
        self.__is_recording = False
