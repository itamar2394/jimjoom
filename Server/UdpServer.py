import socket


class UdpServer:

    """
    This class enables the Udp communication of the server with his clients. It does not have a certain port.
    """

    IP_ADDRESS = "127.0.0.1"  # socket.gethostbyname(socket.gethostname())
    MAXIMUM_MESSAGE_SIZE = 1024 * 12

    def __init__(self, port):
        """
        :param port: port number to listen on.
        udp_socket: the server's socket object.
        address: the ip address of the current connected client.
        """
        self.__port = port
        self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__udp_socket.bind((self.IP_ADDRESS, self.__port))
        self.__address = None

    def receive(self):
        """
        Waits until message will be received from *any* client. Saves its address.
        :return: the message we got, in bytes.
        """
        try:
            message, self.__address = self.__udp_socket.recvfrom(UdpServer.MAXIMUM_MESSAGE_SIZE)
            return message
        except ConnectionResetError:
            pass

    def send(self, message):
        """
        Sends a given message to *the last client we got message from*.
        :param message: message in bytes to send the client.
        """
        if self.__address:
            self.__udp_socket.sendto(message, self.__address)

    def close_connection(self):
        """
        Closes that Udp server.
        """
        self.__udp_socket.close()
