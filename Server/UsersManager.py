from User import User


class UsersManager:

    """
    This class is responsible for the connection to the system and the management of the the connected users.
    """

    def __init__(self, db_manager):
        """
        :param db_manager: DbManager object in order to allow access to users info saved in the db.
        self.__connected_users: dictionary with every current connected user. format {username: User object}
        """
        self.__db_manager = db_manager
        self.__connected_users = {}

    def add_to_connected_users(self, user):
        self.__connected_users[user.get_username()] = user

    def remove_from_connected_users(self, username):
        if username in self.__connected_users.keys():
            self.__connected_users.pop(username)

    def is_connected(self, username):
        return username in self.__connected_users.keys()

    def get_user(self, username):
        """
        Get User object by his username. If the user is connected we will get it from the connected users dict.
        if not (i.e the function was called by the sign_up function), we will access the db, take the required info,
        create a user object and return it.
        :param username: username of the wanted user.
        :return: User object.
        """
        if username in self.__connected_users.keys():
            return self.__connected_users[username]
        data_about_user = self.__db_manager.find_row("Users", "username", username)[0]
        (user_id, username, password, location, instrument, genres, friends) = data_about_user
        genres = genres.split(",")
        return User(username, location, instrument, genres, friends)

    def is_such_username(self, username, **kwargs):
        """
        The method uses the db manager method which finds if a value is exist in some row in specific column, in order
        to check if there's any row with a given username as the 'username' column's value. If the list of results
        returned from the function is empty, we would know the username is not in use yet. (note: the boolean value of
        an empty list is False.)
        :param username: username for check
        :return: "TRUE" if exists, "FALSE" if do no. does not use boolean values since the function is mainly used by
        the tcp handler, and the returned value would pass through socket (has to be byte string).
        """
        if self.__db_manager.find_row("Users", "username", username):
            return "TRUE"
        return "FALSE"

    def close_client(self, username, **kwargs):
        """
        Response function that takes place when client wants to close the jimjoom program. It is the client
        responsibility to leave/cancel first the state he is in (cancel waiting for random jam for example). The
        function removes the user form the connected users list in the users_manager.
        :param username: username of the user who is asking to disconnect.
        :param kwargs: client object. not in use.
        :return: confirmation of the action.
        """
        self.remove_from_connected_users(username)
        return "DONE_SUCCESSFULLY"

    def log_in(self, username, password, **kwargs):
        """
        Response function that is called every time someone tries to connect the system. We will check if the username
        exists and if this his password and if he is not already connected in other device and return corresponding
        answer. If the connection is permitted, we will also create a User object and store it in the connected users
        dict.
        :param username: string with username.
        :param password: string with matching password.
        :param kwargs: client object. not in use.
        :return: If the details are correct we will return "TRUE" along with the user's friends list. If they are not
        we will return "FALSE" answer. If the details is true but the user is already in use we will return "FALSE"
        with explanation.
        """
        result = self.__db_manager.find_row("Users", "username", username)
        if result:
            user_info = result[0]  # can't be more than a single match.
            (user_id, username, real_password, location, instrument, genres, friends) = user_info
            if password == real_password:
                if username in self.__connected_users.keys():
                    return "FALSE:ALREADY_IN_USE"
                friends = friends[:-1]  # take down the comma at the end
                user = User(username, location, instrument, genres.split(","), friends.split(","))
                self.add_to_connected_users(user)
                friends = friends.replace(",", "+")
                response = "TRUE:" + friends
                return response
        return "FALSE:None"

    def create_account(self, username, password, location, instrument, genres, **kwargs):
        """
        Response function that is called every attempt to create a new account. If the given username is not already in
        use, we will store the new user data in the db and create a new User object for the connected_users dict.
        :param username: username string.
        :param password: hashed password string.
        :param location: name of the user's country.
        :param instrument: the user's instrument.
        :param genres: the user's favorite genres.
        :param kwargs: client object. not in use.
        :return: confirmation for the action/message explaining that the username is already in use.
        """
        if self.is_such_username(username) == "TRUE":
            return "USERNAME_ALREADY_EXISTS"
        else:
            genres = genres.replace("+", ",")
            friends = ""
            self.__connected_users[username] = User(username, location, instrument, genres.split(","), friends)
            self.__db_manager.insert("Users", self.__db_manager.get_columns("Users")[1:], [(username, password,
                                                                                            location, instrument,
                                                                                            genres, friends)])
            return "DONE_SUCCESSFULLY"

    def start_recording(self, username, **kwargs):
        """
        Response function that starts/continues the recording for given user.
        :param username: username of the user.
        :param kwargs: client object. not in use.
        :return: confirmation for the action.
        """
        self.__connected_users[username].start_recording()
        return "DONE_SUCCESSFULLY"

    def stop_recording(self, username, **kwargs):
        """
        Response function that stops the recording for given user.
        :param username: username of the user.
        :param kwargs: client object. not in use.
        :return: confirmation for the action.
        """
        self.__connected_users[username].stop_recording()
        return "DONE SUCCESSFULLY"

    def update_friends(self, host_name, user_names):
        """
        The function adds to the friends list of the user in the db the given user names. The function will first check
        who are his friends are and add only the new ones, if there are such.
        :param host_name: user to add friends to.
        :param user_names: list of user names to add.
        """
        current_friends = self.__db_manager.find_row("Users", "username", host_name)[0][-1].split(",")
        friends_to_add = list(filter(lambda username: username not in current_friends, user_names))
        if friends_to_add:
            self.__db_manager.add_data_to_field("Users", "username", host_name, "friends", ",".join(friends_to_add)+",")
