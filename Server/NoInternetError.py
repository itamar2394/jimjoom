import requests


class NoInternetError(Exception):

    @staticmethod
    def check_internet_connection():
        """
        The function checks if the computer is connected to the internet, using HTTP request to Google.com.
        :return: boolean value whether the device is connected to the internet.
        """
        try:
            requests.get('https://www.google.com')
            return True
        except requests.exceptions.ConnectionError:
            return False
