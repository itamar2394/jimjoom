import logging


class Logger:

    """
    This class allows to manage a log file.
    """

    def __init__(self, logger_name):
        self.__logger = logging.getLogger(logger_name)

    def initialize(self, formatter, file_handler):
        """
        Initialize the logger settings.
        :param formatter: format for logs.
        :param file_handler: file to enter logs to.
        :return: the initialized Logger object.
        """
        self.__logger.setLevel(logging.INFO)
        formatter = logging.Formatter(formatter)
        try:
            handler = logging.FileHandler(file_handler)
            handler.setFormatter(formatter)
            self.__logger.addHandler(handler)
        except PermissionError:
            pass
        return self

    def log(self, message):
        """
        logs a message.
        :param message: message to log.
        """
        self.__logger.info(message)
