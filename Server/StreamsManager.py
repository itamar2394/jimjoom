from pydub import AudioSegment
import wave
import os
from contextlib import closing
from threading import Thread
from UdpServer import UdpServer


class StreamsManager:

    """
    This class enables streams management for every active jam.
    """

    SAMPLE_WIDTH = 2
    NUMBER_OF_CHANNELS = 1
    SAMPLE_RATE = 44100
    CHUNK = 1024 * 6

    def __init__(self, users_manager):
        """
        :param users_manager: UsersManager object.
        self.__jams_streams: the current audio segment in bytes for every user in jam right now.
        self.__merged_audio: the current mixed audio segment for every *recording* user, except his own audio.
        self.__udp_servers: the udp object socket for every user currently in jam, for data sending and receiving.
        """
        self.__users_manager = users_manager
        self.__jams_streams = {}
        self.__merged_audio = {}
        self.__udp_servers = {}

    def handle_jam_streams(self, jam):
        """
        The function gets Jam object and takes care of the streaming for all connected users until the moment jam is
        over. The function creates two threads for every user - sending data and receiving data.
        :param jam: Jam object to handle.
        """
        self.__jams_streams[jam] = {user: None for user in jam}
        for user in jam:
            if self.__users_manager.is_connected(user.get_username()):
                self.__udp_servers[user] = UdpServer(user.get_port())
                receive_stream_thread = Thread(target=self.receive_stream, args=[jam, user])
                send_stream_thread = Thread(target=self.send_stream, args=[jam, user])
                receive_stream_thread.start()
                send_stream_thread.start()

    def receive_stream(self, jam, user):
        """
        The function gets User object and receives his audio stream as long as he's connected to the jam. the function
        stores the data in the jams_streams dictionary. If the user is recording we will also create a total mix
        including the user own audio and save it on the recording file.
        :param jam: Jam object of the User.
        :param user: User object to receive data from.
        """
        while len(jam.get_connected_participants()) > 1 and user.is_in_jam():
            self.__jams_streams[jam][user] = self.__udp_servers[user].receive()
            if user.is_recording():
                merged_audio_including_me = self.merge([self.__merged_audio[user], self.__jams_streams[jam][user]])
                self.save_data(merged_audio_including_me, self.get_recording_path(user.get_username()))
        self.__udp_servers[user].close_connection()
        self.__udp_servers.pop(user)

    def send_stream(self, jam, my_user):
        """
        The function gets User and sends him the his mixed audio - the audio segments of all connected users in the jam
        except from him. this, as long as he connected to the jam. If the user is recording we will also save that mix
        in the merged_audio dictionary.
        :param jam: Jam object of the User.
        :param my_user: User object to send data to.
        """
        while len(jam.get_connected_participants()) > 1 and my_user.is_in_jam():
            merged_audio = StreamsManager.merge([self.__jams_streams[jam][current_user] for current_user in jam
                                                if current_user != my_user and self.__jams_streams[jam][current_user]
                                                and self.__users_manager.is_connected(current_user.get_username())])
            if my_user.is_recording():
                self.__merged_audio[my_user] = merged_audio
            self.__udp_servers[my_user].send(merged_audio)

    def has_recorded(self, username):
        """
        :param username: username for check.
        :return: boolean value whether this user had recorded at some point in this jam.
        """
        path_to_recording_file = self.get_recording_path(username)
        return os.path.exists(path_to_recording_file)

    def get_recording_path(self, username):
        """
        :param username: username of the user we want his recording path.
        :return: path to the file in which his recording his saved.
        """
        user = self.__users_manager.get_user(username)
        file_name = user.get_recording_file_name()
        return f"Recordings/{file_name}.wav"

    def receive_recording(self, username, sequence_number, is_last_flag, **kwargs):
        """
        Response function to get chunk from the recording of a user. The function will calculate
        the bytes range needed according the sequence number and every chunk size. if this is the last
        flag we will remove the recording file from the server.
        :param username: username of the user who asks his recording.
        :param sequence_number: the wanted chunk's serial number in the recording.
        :param is_last_flag: boolean value whether this is the last chunk.
        :param kwargs: client object. not in use.
        :return: audio chunk in bytes.
        """
        sequence_number = int(sequence_number)
        path_to_recording_file = self.get_recording_path(username)
        with closing(wave.open(path_to_recording_file, 'rb')) as recording:
            recording.setpos(sequence_number * StreamsManager.CHUNK // 2)
            recording_chunk = recording.readframes(StreamsManager.CHUNK // 2)
        if is_last_flag == "True":
            os.remove(path_to_recording_file)
        return recording_chunk

    def delete_recording(self, username, **kwargs):
        """
        Response function to delete the recording of a user who closed the program while playing in a jam. Usually
        recordings gets deleted after the user done receiving them - but here he won't receive them so he should ask
        for that explicitly. This function is used as well by users who are closing the program in the middle of the
        recording receiving process.
        :param username: username of the user who wants to delete his saved recording on the server.
        :param kwargs: client info.
        :return: a confirmation.
        """
        os.remove(self.get_recording_path(username))
        return "DONE_SUCCESSFULLY"

    def get_recording_size(self, username):
        """
        The function used to return the recording size for user who left the jam. That way he would know in how many
        chunks he should ask for it.
        :param username: username of the user we want to get his recording size.
        :return: his recording bytes size.
        """
        path_to_recording_file = self.get_recording_path(username)
        try:
            with closing(wave.open(path_to_recording_file)) as recording:
                return len(recording.readframes(recording.getnframes()))
        except EOFError:
            return

    @staticmethod
    def save_data(audio_bytes, path_to_recording_file):
        """
        The function gets data and path to file and adds that data to the file. since the file is a wave file and the
        wave package does not support opening in 'append' mode, we'll have to open and read the file content and then
        rewrite it with its original content after we added it the new additional bytes.
        :param audio_bytes: bytes to add.
        :param path_to_recording_file: path to file the bytes will added to.
        :return:
        """
        if os.path.exists(path_to_recording_file):
            with closing(wave.open(path_to_recording_file, 'rb')) as current_recording:
                length = current_recording.getnframes()
                current_data = current_recording.readframes(length)
            audio_bytes = current_data + audio_bytes
        with closing(wave.open(path_to_recording_file, "wb")) as recording_file:
            recording_file.setnchannels(StreamsManager.NUMBER_OF_CHANNELS)
            recording_file.setsampwidth(StreamsManager.SAMPLE_WIDTH)
            recording_file.setframerate(StreamsManager.SAMPLE_RATE)
            recording_file.writeframesraw(audio_bytes)

    @staticmethod
    def merge(list_of_audio_segments):
        """
        creates an audio mix from list of audio segments in bytes. For each one we will create AudioSegment object and
        using his method 'overlay' and a simple for loop, we will add to the first object all the other.
        :param list_of_audio_segments: list of audio segments in bytes.
        :return: bytes of merged audio.
        """
        audio_segment_objects = [AudioSegment(data,
                                              sample_width=StreamsManager.SAMPLE_WIDTH,
                                              frame_rate=StreamsManager. SAMPLE_RATE,
                                              channels=StreamsManager.NUMBER_OF_CHANNELS)
                                 for data in list_of_audio_segments if data]
        if audio_segment_objects:
            mixed_audio = audio_segment_objects.pop(0)
            for audio_segment_object in audio_segment_objects:
                if bytes(audio_segment_object.raw_data):
                    mixed_audio = mixed_audio.overlay(audio_segment_object)
            return bytes(mixed_audio.raw_data)
        return b""
