import mysql.connector


class WrongConnectionDetails(Exception):
    pass


class DbManager:

    """
    The class connects to the db and allows easy access to it including reading, writing and search functions.
    """

    def __init__(self, username, password, db_name, db=None):
        """
        :param username: db username.
        :param password: db password.
        :param db: db name.
        """
        self.__username = username
        self.__password = password
        self.__db_name = db_name
        self.db = db
        self.__cursor = self.__db.cursor(buffered=True)

    @property
    def db(self):
        return self.__db

    @db.setter
    def db(self, db):
        if not db:
            try:
                self.__db = mysql.connector.connect(host="localhost", user=self.__username, password=self.__password,
                                                    database=self.__db_name)
            except mysql.connector.errors.ProgrammingError:
                raise WrongConnectionDetails
        elif isinstance(db, mysql.connector.MySQLConnection):
            self.__db = db
        else:
            raise WrongConnectionDetails

    def create_table(self, table_name, columns):
        """
        creates a table on the instance data base, by given table name and columns info.
        :param table_name: name of the new table the function would create
        :param columns: dictionary of with {names of the column: data type for the column}
        """
        command = f"CREATE TABLE {table_name} ("
        for column, data_type in columns.items():
            command += f"{column} {data_type}, "
        command = command[:-2]
        command += ")"
        try:
            self.__cursor.execute(command)
        except mysql.connector.errors.ProgrammingError:
            return

    def get_columns(self, table_name):
        """
        Returns list of the columns names for a given table name.
        :param table_name: the table name that its columns list is wanted.
        :return: list of columns names.
        """
        command = f"DESCRIBE {table_name}"
        try:
            self.__cursor.execute(command)
        except mysql.connector.errors.ProgrammingError:
            return
        return [column_info[0] for column_info in self.__cursor.fetchall()]

    def insert(self, table_name, columns, values):
        """
        insert to a given table new row with the given values list.
        :param table_name: name of the table we will insert to.
        :param columns: list of the columns names we want to set value for.
        :param values: *matching* values list for every wanted column.
        :return:
        """
        values_string = "%s, " * (len(columns) - 1) + "%s"
        columns_string = ", ".join(columns)
        command = f"INSERT INTO {table_name} ({columns_string}) VALUES ({values_string})"
        try:
            self.__cursor.executemany(command, values)
            self.__db.commit()
        except mysql.connector.errors.ProgrammingError:
            return

    def select(self, table_name):
        """
        :param table_name: string with the name of the wanted table.
        :return:two dimensional list representing the table.
        """
        try:
            self.__cursor.execute(f"SELECT * FROM {table_name}")
        except mysql.connector.errors.ProgrammingError:
            return
        return self.__cursor.fetchall()

    def find_row(self, table_name, column_name, value):
        """
        Returns a list with all rows that meets given search demand.
        :param table_name: table to search in.
        :param column_name: the column according to which the search will be performed.
        :param value: the value we are searching in the given column.
        :return: all matching rows (list of lists).
        """
        command = f"SELECT * FROM {table_name} WHERE {column_name} ='{value}'"
        try:
            self.__cursor.execute(command)
        except mysql.connector.errors.ProgrammingError:
            return
        return self.__cursor.fetchall()

    def add_data_to_field(self, table_name, column_for_identification, value_for_identification, column_to_update,
                          data_to_add):
        """
        Add data to specific field in the db. find the wanted row by column-value search and updates the value in other
        given column.
        :param table_name: table in which the update would occur.
        :param column_for_identification: the column of the wanted field for change.
        :param value_for_identification: the value we are expecting to find in that column.
        :param column_to_update: the column in which we want to update the value.
        :param data_to_add: the value we want to add.
        :return:
        """
        command = f"UPDATE {table_name} SET {column_to_update} = CONCAT({column_to_update}, '{data_to_add}') WHERE" \
                  f" {column_for_identification} = '{value_for_identification}'"
        try:
            self.__cursor.execute(command)
            self.__db.commit()
        except mysql.connector.errors.ProgrammingError:
            return

    def print_sweet_table(self, table_name):
        """
        prints in organized way a given table.
        :param table_name: table to print.
        """
        columns = self.get_columns(table_name)
        table_data = self.select("Users")
        if table_data is None:
            print("create 'Users' table first")
            return
        matrix = [columns] + table_data
        if len(matrix) == 1:
            return
        s = [[str(e) if e else "None" for e in row] for row in matrix]
        lens = [max(map(len, col)) for col in zip(*s)]
        fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
        table = [fmt.format(*row) for row in s]
        print('\n'.join(table))
