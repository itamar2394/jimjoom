class Updates:
    def __init__(self, tcp_server):
        """
        This class is a collection of update functions. Updates functions responsible to send the response for a waiting
        request. They will be called when the thing that the client has been waiting for had occurred.
        :param tcp_server: TcpServer object to send the updates through
        """
        self.__tcp_server = tcp_server

    def update_the_host_that_is_waiting_for_invited_users(self, client, username="None", update="CONNECT"):
        """
        Updates the host of a private unopened jam that some invited user had connected or left.
        :param client: the host's client socket object for update.
        :param username: username of the invited user who joined/left.
        :param update: the type of update. default is CONNECT.
        """
        self.__tcp_server.send_response(client, f"{update}:{username}")
        self.__tcp_server.close_connection(client)

    def update_participants_about_jam_changes(self, update, jam_id, username, client_objects_for_jam_updates):
        """
        Updates all connected users in given jam about the leaving/joining of some user.
        :param update: the type of update.
        :param jam_id: the jam we need to update its members.
        :param username: the username of the user the update is about.
        :param client_objects_for_jam_updates: dictionary with the socket objects who are waiting for update.
        """
        for current_username, client in client_objects_for_jam_updates[jam_id].items():
            if client:
                if current_username == username:
                    self.__tcp_server.send_response(client, f"YOU_LEFT:None")
                else:
                    self.__tcp_server.send_response(client, f"{update}:{username}")

    def update_other_participants_that_jam_is_starting(self, connected_users, waiting_for_the_host):
        """
        Updates all connected users who has joined given private jam, that the jam was started by the host.
        :param connected_users: string with the connected users user names, separated by commas.
        :param waiting_for_the_host: iterator with clients socket object for response.
        """
        for client in waiting_for_the_host:
            self.__tcp_server.send_response(client, f"JAM_STARTED:{connected_users}")
            self.__tcp_server.close_connection(client)

    def inform_the_waiting_for_host_that_the_jam_was_closed_by_the_host(self, waiting_for_host, jam_id):
        """
        Updates users who connected to unopened private jam that the jam was canceled by the host.
        :param waiting_for_host: dictionary with socket objects of the waiting for the host.
        :param jam_id: the jam id of the jam that was cancelled.
        """
        if jam_id in waiting_for_host.keys():
            for client in waiting_for_host[jam_id].values():
                self.__tcp_server.send_response(client, "JAM_WAS_CLOSED_BY_THE_HOST")
                self.__tcp_server.close_connection(client)

    def update_user_that_he_stopped_waiting_for_random_jam(self, client):
        """
        Updates user that he had stopped waiting for random jam and that way closes the previous request.
        :param client: client to update.
        """
        self.__tcp_server.send_response(client, "YOU_HAVE_LEFT_THE_RANDOM_JAM_WAITING_LIST")
        self.__tcp_server.close_connection(client)
