import random


class Jam:

    """
    This class representing a group of User objects. It's created for every new jam.
    """

    MIN_ID_VALUE = 1000
    MAX_ID_VALUE = 6553

    def __init__(self, participants, jams_manager, jam_id=None):
        """
        :param participants: list of User objects.
        :param jams_manager: the JamManager object that is responsible for this jam.
        :param jam_id: the jam's numeric identifier.
        """
        self.__participants = participants
        self.__jams_manager = jams_manager
        self.__is_active = False
        self.jam_id = jam_id
        self.__host = None
        self.__is_private = False

    @property
    def jam_id(self):
        return self.__jam_id

    @jam_id.setter
    def jam_id(self, jam_id):
        """
        Takes advantage of the encapsulation qualities in order to make sure jam id is valid. meaning: it is not already
        in use. If it does, we will generate new random id in the range we defined, until we'll get one that isn't
        currently used. The function will set the new id as an private attribute __jam_id.
        :param jam_id: the jam id we got as a parameter for this jam.
        """
        if jam_id in self.__jams_manager.get_ids_in_use() or not jam_id:
            while True:
                jam_id = random.randint(Jam.MIN_ID_VALUE, Jam.MAX_ID_VALUE)
                if jam_id not in self.__jams_manager.get_ids_in_use():
                    break
        self.__jam_id = str(jam_id)

    def get_participants(self):
        return self.__participants

    def host(self):
        return self.__host

    def set_host(self, username):
        self.__host = username

    def set_as_private(self):
        self.__is_private = True

    def is_private(self):
        return self.__is_private

    def set_as_active(self):
        self.__is_active = True
        for user in self:
            user.set_is_in_jam(True, jam=self)

    def is_active(self):
        return self.__is_active

    def find_user_by_username(self, username):
        """
        Used to easily get the user object, of someone in this jam, just by his username.
        :param username: a user's username.
        :return:
        """
        for user in self:
            if user.get_username() == username:
                return user

    def is_user_in_jam(self, username):
        """
        This function helps find out whether a username belongs in this jam. The functions is used when someone asks to
        join a private jam and we want to check if he's invited to it.
        :param username: a user's username.
        :return: whether there is a user in this jam that this is his username.
        """
        return username in [user.get_username() for user in self]

    def remove_participant(self, username):
        """
        The functions gets username and removes it from the jam's currently connected users list. If after that action
        there is no connected users, the jam will be ended.
        :param username: the username of the user we want to remove from this jam.
        """
        user = self.find_user_by_username(username)
        user.set_is_in_jam(False)
        self.__participants.remove(user)
        if len(self.get_connected_participants()) == 0:
            self.__is_active = False
            self.__jams_manager.end_jam(self.jam_id)

    def get_connected_participants(self):
        """
        The functions returns list of the user names of the currently connected users. Notice! this is intended only for
        private jam. For random jam the function will return list of all participants.
        :return: list of user names on the connected users. If not private - list of al users.
        """
        if not self.__is_private:
            return self.__participants
        return [user.get_username() for user in self if self.__jams_manager.is_connected(self.__jam_id,
                                                                                         user.get_username())]

    def get_data_about_jam(self):
        """
        Get the username-instrument-location set of data for each user in the jam. that information will be passed,
        about every user, to every user when he joins the jam.
        :return: string of data. each users is separated by plus sign; each data section is separated by comma.
        """
        data_about_jam = ""
        for user in self:
            data_about_jam += f"{user.get_username()}-{user.get_instrument()}-{user.get_location()}+"
        data_about_jam = data_about_jam[:-1]
        return data_about_jam

    def __str__(self):
        return str([user.get_username() for user in self])

    def __iter__(self):
        return iter(self.__participants)

    def __len__(self):
        return len(self.__participants)
