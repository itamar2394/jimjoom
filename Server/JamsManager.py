from Jam import Jam
from Updates import Updates
from datetime import datetime
from threading import Thread


class JamsManager:

    """
    The class is in charge of the creation, the closure and the regular activity of all jams from all types. It offers
    the ability to alter jams and access the Jam objects. It organizes the users by their current situation on the way to
    the jam. It is also responsible for the different waiting states and hence it holds client objects for future
    response.
    """

    MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM = 3
    MAXIMUM_PARTICIPANTS_NUMBER_FOR_JAM = 4

    def __init__(self, users_manager, tcp_server, streams_manager):
        """
        self.__current_jams: {jam_id: Jam object}; used to contain all current jams, those which started and those which
         aren't yet.
        self.__waiting_for_random_jam_dict: {user: {"client": client object, "time connected": datetime object}};
        dictionary of dictionaries, for every user that is currently waiting for random jam. every user's value contains
        the client which made the request, for future response, when jam would be available, and a time object of when
        the request has occurred, in order to allow smart random-not-so-random jams choice.
        self.__invited_users_that_did_not_connect_yet: {jam id: = {"user_names": user names, "host client":
        client object}}; for every private jam, contains list of invited user names the didn't join yet. also includes
        the host's client object in order to update him every time someone join/left. notice: all of that is just about
        before jam has started! note: this attribute's name is not so accurate, since someone can connect the jam and
        the leave it but still he would be included.
        self.__waiting_fo_host_clients: {jam id: {username: client object}}; in a similar way, holds the client for
        every participant who joined a private jam and waits for it to start, that's in order to update him when the jam
        would start/would be canceled.
        self.__client_objects_for_jam_updates: {jam id: {username: client object}}; exactly the same as the waiting for
        host client, just with different purpose, to update the users about jam changes when the jam has already
        started. (someone left, someone joined, jam is over, ect.)
        """
        self.__users_manager = users_manager
        self.__tcp_server = tcp_server
        self.__streams_manager = streams_manager
        self.__updates = Updates(self.__tcp_server)
        self.__current_jams = {}
        self.__waiting_for_random_jam_dict = {}
        self.__invited_users_that_did_not_connect_yet = {}
        self.__waiting_for_host_clients = {}
        self.__client_objects_for_jam_updates = {}

    def get_waiting_for_host(self):
        return self.__waiting_for_host_clients

    def get_client_objects_for_jam_updates(self):
        return self.__client_objects_for_jam_updates

    def get_tcp_server(self):
        return self.__tcp_server

    def get_invited_users_that_did_not_connect_yet(self):
        return self.__invited_users_that_did_not_connect_yet

    def get_jam_by_id(self, jam_id):
        return self.__current_jams[jam_id]

    def get_waiting_for_random_jam_dict(self):
        return self.__waiting_for_random_jam_dict

    def is_connected(self, jam_id, username):
        return username not in self.__invited_users_that_did_not_connect_yet[jam_id]["user_names"]

    def remove_user_from_client_objects_for_jam_updates(self, jam_id, username):
        self.__client_objects_for_jam_updates[jam_id][username] = None

    def remove_jam_from_waiting_for_host(self, jam_id):
        """
        remove private jam's invited users info from the waiting_for_host list. that occurs when the jam starts.
        :param jam_id: jam id of the jam we want to remove.
        """
        self.__waiting_for_host_clients.pop(jam_id)

    def get_ids_in_use(self):
        """
        returns list of jam id's that are currently in use. since the jams id's are randomly generated, we can avoid
        collisions that way.
        """
        return [jam.jam_id for jam in self.__current_jams.values()]

    def remove_from_invited_users_that_did_not_connect_yet(self, jam_id, username):
        """
        removes username from the 'disconnected from waiting-to-open jam' dictionary. used when someone had joined the
        jam.
        """
        self.__invited_users_that_did_not_connect_yet[jam_id]["user_names"].remove(username)

    def add_to_invited_users_that_did_not_connect_yet(self, jam_id, username):
        """
        add username to the 'disconnected from waiting-to-open jam' dictionary. used for all invited users when a
        private jam is created, or when someone who joined had left.
        """
        self.__invited_users_that_did_not_connect_yet[jam_id]["user_names"].append(username)

    def create_jam(self, participants):
        """
        The method creates new jam, adds it to the current jams list and initialize the jam's place in the update
        clients dictionary, which will be filled as the users would join. It takes care of setting each participant's
        port as well.
        :param participants: list of User objects
        :return: Jam object
        """
        new_jam = Jam(participants, self)
        jam_id = new_jam.jam_id
        self.__current_jams[jam_id] = new_jam
        self.__client_objects_for_jam_updates[jam_id] = {user.get_username(): None for user in
                                                         new_jam.get_participants()}
        for i, user in enumerate(new_jam):
            user.set_port(int(jam_id+str(i)))
        return new_jam

    def create_private_jam(self, *args, **kwargs):
        """
        A response function that's used when someone creates a private jam. The function creates the jam using the
        'create jam' method and returns some info about it, for the host's client use. It updates the friends list of
        the host in the Db and adds to it every user he had invited. that way the client will be able to suggest to add
        them when the host would create another private jam.
        :param args: list of user names for the new jam
        :param kwargs: client object that every response function gets, not used in here.
        :return: string with data about the new jam, including jam id, personal port for the host and users info -
        location and instrument.
        """
        user_names = list(args)
        users = [self.__users_manager.get_user(username) for username in user_names]
        jam = self.create_jam(users)
        host_username = user_names.pop(-1)
        jam.set_as_private()
        jam.set_host(host_username)
        self.__users_manager.update_friends(host_username, user_names)
        self.__invited_users_that_did_not_connect_yet[jam.jam_id] = {"user_names": user_names,
                                                                     "host_client": None}
        port = jam.get_participants()[-1].get_port()  # host appears last
        data_about_jam = f"{jam.jam_id}:{str(port)}:{jam.get_data_about_jam()}"
        return data_about_jam

    def join_private_jam(self, username, jam_id, **kwargs):
        """
        A response functions that used when someone asks to join a private jam. The functions checks whether he belongs
        in that jam and returns suitable response. There are three options. 1. He doesn't belong in the jam, meaning -
        the jam is not private, the jam is private, but he wasn't invited or there isn't such jam - then he would get
        response that start with "FALSE". 2. He belongs in the jam and the jam has not started yet - he would get
        message that starts with "TRUE", his personal port and users' info. In this case the function will also remove
        him from the 'invited users that didn't connect yet' list and make sure to update the host that someone had
        joined.  3. He belongs in the jam and the jam started already - he would get "ACTIVE" response together with
        port, users' info and the list of current connected users, so the client would be able to represent them
        differently in the jam screen. Also in this case the function remove him from the 'invited users that didn't
        connect yet' list, but this time it will update all jam users about the change as well, so they would know he
        had joined.
        :param username: username of the user who is asking.
        :param jam_id: the jam id of the wanted jam.
        :param kwargs: client object, not in use.
        :return: string with appropriate response as detailed above.
        """
        if self.__current_jams:
            if jam_id in self.__current_jams.keys():
                jam = self.__current_jams[jam_id]
                if jam.is_user_in_jam(username) and jam.is_private():
                    user = jam.find_user_by_username(username)
                    port = user.get_port()
                    self.remove_from_invited_users_that_did_not_connect_yet(jam_id, username)
                    if jam.is_active():
                        update_thread = Thread(target=self.__updates.update_participants_about_jam_changes,
                                               args=["SOMEONE_JOINED", jam_id, username,
                                                     self.__client_objects_for_jam_updates])
                        update_thread.start()
                        return f"ACTIVE:" \
                               f"{str(port)}:{jam.get_data_about_jam()}:{','.join(jam.get_connected_participants())}"
                    host_client = self.__invited_users_that_did_not_connect_yet[jam_id]["host_client"]
                    self.__updates.update_the_host_that_is_waiting_for_invited_users(host_client, username=username)
                    return f"TRUE:{str(port)}:{jam.get_data_about_jam()}:NONE"
        return "FALSE:NONE:NONE:NONE"

    def wait_for_host(self, jam_id, username, **kwargs):
        """
        A future response function that is requested by someone who had joined a private jam that hasn't started yet.
        That someone is waiting for an update when the jam would start/be canceled. The function saves the client object
        that stored in a key argument and saves it on the 'waiting for host clients' dictionary.
        :param jam_id: jam id of the jam the user is waiting for.
        :param username: username of the user who is waiting.
        :param kwargs: client object, used for future response.
        :return: None. return means response and we don't want to response at this moment.
        """
        if jam_id in self.__waiting_for_host_clients.keys():
            self.__waiting_for_host_clients[jam_id][username] = kwargs["client"]
        else:
            self.__waiting_for_host_clients[jam_id] = {username: kwargs["client"]}

    def start_jam(self, jam_id, **kwargs):
        """
        response function that is called when the host of a private jam asks to start his jam. This function relates
        only with private jams and not random ones.
        :param jam_id: the jam id of the jam which is asked to start.
        :param kwargs: data about client, not in use here.
        :return: list of the connected users, so the host will know which users are actually in the jam.
        """
        jam = self.get_jam_by_id(jam_id)
        jam.set_as_active()
        connected_users = ",".join(jam.get_connected_participants())
        return connected_users

    def leave_jam(self, username, jam_id, **kwargs):
        """
        response function that is called when someone asks to leave the jam he is currently in.
        :param username: the username of the user who asks to leave the jam.
        :param jam_id: the jam id of the jam the user wants to leave.
        :param kwargs: client object. not in used.
        :return: if the user did record during this jam, we will response with the size of the recording, so the client
        will know in how many chunks he should ask it. if he did not record, we will just answer with a confirmation to
        the action.
        """
        user = self.__users_manager.get_user(username)
        jam = self.__current_jams[jam_id]
        jam.remove_participant(username)
        if self.__streams_manager.has_recorded(username):
            if user.is_recording():
                user.stop_recording()
            size = self.__streams_manager.get_recording_size(username)
            if size:
                return str(size)
        return "DONE_SUCCESSFULLY"

    def end_jam(self, jam_id):
        """
        When all users left the jam, we will use this function to end the jam. to end jam means removing it from the
        current jams dictionary.
        :param jam_id: jam id of the jam we want to end.
        """
        if jam_id in self.__current_jams.keys():
            self.__current_jams.pop(jam_id)

    def add_to_random_jam_waiting_list(self, username, **kwargs):
        """
        a waiting response function that is called when someone asks to join a random jam. we will wait until there
        would be such available jam (reach the MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM limit).
        meanwhile, we just have to save the client object in the waiting_for_random_jam dictionary for future response.
        we will also call the handle_random_jams function in order to check if now, when another user had joined the
        waiting list, there would be such available jam.
        :param username: username of the user who is asking to join a random jam.
        :param kwargs: client object.
        """
        user = self.__users_manager.get_user(username)
        self.__waiting_for_random_jam_dict[user] = {"client": kwargs["client"], "time connected": datetime.now()}
        self.handle_random_jams()

    def stop_waiting_for_random_jam(self, username, **kwargs):
        """
        response function that removes user from the random jam waiting list.
        :param username: username of the user who asks to stop waiting.
        :param kwargs: client object, not in use.
        :return: confirmation of the action.
        """
        user = self.__users_manager.get_user(username)
        client = self.__waiting_for_random_jam_dict[user]["client"]
        self.__updates.update_user_that_he_stopped_waiting_for_random_jam(client)
        if user in self.__waiting_for_random_jam_dict.keys():
            self.__waiting_for_random_jam_dict.pop(user)
        return "DONE_SUCCESSFULLY"

    def find_genre_match(self, users_list):
        """
        The function helps for the random jam smart staffing. it gets list of user objects, find the most common genre
        among them and returns list of the users that likes that genre.
        from
        :param users_list: list of user objects to perform the search on.
        :return: If the the longest possible new list with users that have one genre in common is longer than the
        MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM we will return that list. if not we will return None (either when
        there no common genre at all).
        """
        users_by_genres = {}
        for user_object in users_list:
            genres = user_object.get_genres()
            for genre in genres:
                if genre in users_by_genres:
                    users_by_genres[genre].append(user_object)
                else:
                    users_by_genres[genre] = [user_object]

        users_with_most_common_genre = sorted(users_by_genres.values(),
                                              key=lambda list_of_users: len(list_of_users))[-1]
        if len(users_with_most_common_genre) >= self.MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM:
            return users_with_most_common_genre

    def is_new_jam_available(self):
        """
        The method is called every time new user joins the random jam waiting list. we will check if now there is
        available jam, i.e., there are more waiting users than the MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM.
        if there are, we will use find_genre_match to find users with similar musical taste. if that list is
        longer than the MAXIMUM_PARTICIPANTS_NUMBER_FOR_JAM we will choose the maximum number allowed of users
        who are waiting the longest period. if there is no common genre that is shared by more than the
        MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM, then we will just choose those who asked earlier. their number will
        be, just as before, the largest number that is allowed by the maximum limit, if we reach that limit. if not, it
        will be all the waiting users. after we chose the new jam members, we will create new Jam object.
        :return: if jam is available, we will return True along with the Jam object we created and the members' client
        objects for update. if not we will return False (and None for the additional info).
        """
        if len(self.__waiting_for_random_jam_dict) >= self.MINIMUM_PARTICIPANTS_NUMBER_FOR_RANDOM_JAM:
            users = list(self.__waiting_for_random_jam_dict.keys())
            chosen_users = self.find_genre_match(users)
            if not chosen_users:
                chosen_users = sorted(self.__waiting_for_random_jam_dict.keys(),
                                      key=lambda user_object:
                                      self.__waiting_for_random_jam_dict[user_object]["time connected"])
            else:
                chosen_users.sort(key=lambda user_object:
                                  self.__waiting_for_random_jam_dict[user_object]["time connected"])
            chosen_users = chosen_users[:self.MAXIMUM_PARTICIPANTS_NUMBER_FOR_JAM]
            clients = []
            for user in chosen_users:
                clients.append(self.__waiting_for_random_jam_dict[user]["client"])
                self.__waiting_for_random_jam_dict.pop(user)
            jam = self.create_jam(chosen_users)
            jam.set_as_active()
            return True, jam, clients
        return False, None, None

    def wait_for_invited_users(self, jam_id, **kwargs):
        """
        Waiting response function for hosts who want to receive update when users they have invited would join or leave
        the unopened jam. The function will save their client object in the invited users that did not connect yet, in
        this jam's sub-dict, under the host_client section.
        :param jam_id: jam id of the jam that his host asks for update.
        :param kwargs: client object.
        :return: None. this is future response function, therefore we don't want to return anything now.
        """
        client = kwargs["client"]
        self.__invited_users_that_did_not_connect_yet[jam_id]["host_client"] = client

    def wait_for_jam_updates(self, username, jam_id, **kwargs):
        """
        Waiting response function for users in jam who want to receive updates about changes in the jam - someone joined
        or left. we will just save his client object for future use in clinet_objects_for_jam_updates.
        :param username: the username of the asking user.
        :param jam_id: the jam in which the user is present.
        :param kwargs: client object.
        :return: None. this is future response function, therefore we don't want to return anything now.
        """
        client = kwargs["client"]
        self.__client_objects_for_jam_updates[jam_id][username] = client

    def cancel_jam(self, jam_id, username, **kwargs):
        """
        Response function that's called when some private jam's host is asking to cancel the jam that did not start yet.
        the function removes the host from the jam, ends it and updates those who already connected to it.
        :param jam_id: jam id of the jam the host wants to cancel.
        :param username: the username of the host.
        :param kwargs: client object. not in use.
        :return: confirmation of the action.
        """
        self.leave_jam(username, jam_id)
        self.end_jam(jam_id)
        self.__updates.inform_the_waiting_for_host_that_the_jam_was_closed_by_the_host(self.__waiting_for_host_clients,
                                                                                       jam_id)
        return "DONE_SUCCESSFULLY"

    def disconnect_from_jam_that_did_not_start_yet(self, username, jam_id, **kwargs):
        """
        Response function that takes place when user who joined private jam is asking to leave it, *before* the jam had
        started. We will add him back to the invited_users_that_did_not_connect_yet dictionary.
        :param username: username of the user who is asking to disconnect.
        :param jam_id: the jam id of the jam he is asking to disconnect from.
        :param kwargs: client object. not in use.
        :return: confirmation of the action.
        """
        self.add_to_invited_users_that_did_not_connect_yet(jam_id, username)
        return "DONE_SUCCESSFULLY"

    def additional_updates(self, request):
        """
        This function solves problem I couldn't understand. sometimes, when trying to send a message to other opened
        client object, in the middle of request-response process, an error will occur. For example, if we would try
        and update the users in the jam that some user had left inside the leave_jam response function, i.e., between
        receiving request and sending response functions, we will get an error. That's with no difference between
        update that is intended for the user himself (other previous request which we would want to close by sending
        response) or for other users. What's really weird is that for specific updates that *does* work without any
        problem. anyway, that function is called after every response that is being send and that way, we manage to
        execute the update, it just happens a moment after the original request has been answered. This is a little bit
        complex solution but it works.
        :param request: the request. just as it was received from the client.
        """
        if request.startswith("START_JAM"):
            # updates all connected users in the jam that the jam was started. updates also the host himself (that made
            # the request), in order that the 'waiting for users to join' communication will be closed. also starts the
            # streams handle.
            jam_id = request.split(":")[1]
            connected_users = ",".join(self.get_jam_by_id(jam_id).get_connected_participants())
            self.__updates.update_other_participants_that_jam_is_starting(connected_users,
                                                                          self.__waiting_for_host_clients[jam_id]
                                                                          .values())
            self.remove_jam_from_waiting_for_host(jam_id)
            self.__streams_manager.handle_jam_streams(self.get_jam_by_id(jam_id))
            host_client = self.__invited_users_that_did_not_connect_yet[jam_id]["host_client"]
            self.__updates.update_the_host_that_is_waiting_for_invited_users(host_client, jam_id, update="JAM_STARTED")

        elif request.startswith("LEAVE_JAM"):
            # updates every user that's connected to the jam about the change.
            username, jam_id = request.split(":")[1].split(",")
            self.__updates.update_participants_about_jam_changes("SOMEONE_LEFT", jam_id, username,
                                                                 self.__client_objects_for_jam_updates)
            self.remove_user_from_client_objects_for_jam_updates(jam_id, username)
        elif request.startswith("CANCEL_JAM"):
            # update every users that's connected to the unopened jam about the cancel. updates also the host himself
            # (that made the request), in order that the 'waiting for users to join' communication will be closed.
            jam_id = request.split(":")[1].split(",")[0]
            host_client = self.get_invited_users_that_did_not_connect_yet()[jam_id]["host_client"]
            self.__updates.update_the_host_that_is_waiting_for_invited_users(host_client, update="JAM_CANCELED")
        elif request.startswith("DISCONNECT_FORM_JAM_THAT_DID_NOT_START_YET"):
            # update both host and user himself about the disconnection.
            username, jam_id = request.split(":")[1].split(",")
            host_client = self.get_invited_users_that_did_not_connect_yet()[jam_id]["host_client"]
            self.__updates.update_the_host_that_is_waiting_for_invited_users(host_client, username=username,
                                                                             update="DISCONNECT")
            wait_for_host_client = self.get_waiting_for_host()[jam_id][username]
            self.__tcp_server.send_response(wait_for_host_client, "YOU_DISCONNECTED_THE_JAM")

    def handle_random_jams(self):
        """
        This function takes place every time new user joins the random jam waiting list. We will check if there's an
        new jam available. If there is we will update every user in the jam object we got and start the streams handle.
        :return:
        """
        is_new_jam_available, jam, clients = self.is_new_jam_available()
        if is_new_jam_available:
            print("NEW_JAM:" + str(jam))
            data_about_jam = jam.get_data_about_jam()
            for user, client in zip(jam.get_participants(), clients):
                data_about_jam_with_private_port = f"{str(user.get_port())}:{data_about_jam}"
                self.__tcp_server.send_response(client, data_about_jam_with_private_port)
                self.__tcp_server.close_connection(client)
            self.__streams_manager.handle_jam_streams(jam)

    def __str__(self):
        return "\n".join(self.__current_jams)
