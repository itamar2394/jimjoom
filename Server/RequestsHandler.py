class RequestsHandler:

    """
    the class allows handling of the coming tcp requests.
    """

    def __init__(self, response_functions, logger, tcp_server, jams_manager):
        """
        :param response_functions: dictionary with matching function for every request type the server supports.
        :param logger: Logger object to log each request-response.
        :param tcp_server: TcpServer object to send response through.
        :param jams_manager: JamsManager object (for the additional updates function).
        """
        self.__response_functions = response_functions
        self.__logger = logger
        self.__tcp_server = tcp_server
        self.__jams_manager = jams_manager

    def response(self, request, client):
        """
        The function takes a request string, calls its corresponding response function after the separation of the
        parameters. If a response was returned, we will send it to the client object. If not, (waiting request) we will
        not send anything back to the client. We will also document every request (and response if exists) in the log
        file. If there is not such request type in the server, we will just return the request itself. After we send
        the response we will call (if the request was familiar) the additional_updates method.
        :param request: request in the format (a,b,c are parameters): REQUEST_TYPE:a,b,c
        :param client: socket object of the client to return response for.
        """
        if request.count(":"):
            try:
                action, parameters = request.split(":")
                parameters = parameters.split(",")
            except ValueError:
                response = request
                self.__tcp_server.send_response(client, response)
                self.__tcp_server.close_connection(client)
                return
            if action in self.__response_functions.keys():
                try:
                    response = self.__response_functions[action](*parameters, client=client)
                except TypeError:
                    response = request
                if isinstance(response, str):
                    print(f"{request}   >>>   {response}")
                    self.__logger.log(f"{request}   >>>   {response}")
                else:
                    self.__logger.log(request)
            else:
                response = request
        else:
            response = request
        if response:
            self.__tcp_server.send_response(client, response)
            self.__tcp_server.close_connection(client)
            if response != request:
                self.__jams_manager.additional_updates(request)
