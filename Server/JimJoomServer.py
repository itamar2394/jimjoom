from threading import Thread


class JimJoomServer:

    """
    This class functions as the main thread of the server, for the constant receive of Tcp requests.
    """

    def __init__(self, requests_handler, tcp_server):
        """
        :param requests_handler: RequestHandler object to handle coming requests.
        :param tcp_server: TcpServer object to receive requests through.
        """
        self.__requests_handler = requests_handler
        self.__tcp_server = tcp_server

    def serve_forever(self):
        """
        As long as the server is running, we will wait for request after request and open a new thread for each. In
        that thread, the request_handler will take care of the request until response is sent.
        """
        while True:
            client, client_address = self.__tcp_server.connect_to_client()
            request = self.__tcp_server.receive_request(client)
            response_thread = Thread(target=self.__requests_handler.response, args=[request, client])
            response_thread.start()
