import socket


class TcpServer:

    """
    This class is assigned to enable the Tcp communication with the clients. It uses port 2394.
    """

    IP_ADDRESS = "127.0.0.1"  # socket.gethostbyname(socket.gethostname())
    PORT = 2394
    MAX_MSG_SIZE = 1024 * 6

    def __init__(self):
        """
        Create a socket object and bind it to the local machine.
        """
        self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__tcp_socket.bind((self.IP_ADDRESS, self.PORT))

    def connect_to_client(self):
        """
        Waits until some client would connect the server.
        :return: The client's socket object with his address.
        """
        self.__tcp_socket.listen()
        client, address = self.__tcp_socket.accept()
        return client, address

    @staticmethod
    def receive_request(client):
        """
        Waits for a message from a given socket object.
        :param client: The client's socket object for listening.
        :return: the encoded message we got.
        """
        return client.recv(TcpServer.MAX_MSG_SIZE).decode()

    @staticmethod
    def send_response(client, response):
        """
        Sends a given message to a given client. If the message isn't encoded, we'll encode it first.
        :param client: client's socket object for response.
        :param response: the message to send.
        """
        if isinstance(response, str):
            response = response.encode()
        try:
            client.send(response)
        except (ConnectionResetError, ConnectionAbortedError):
            pass

    @staticmethod
    def close_connection(client):
        """
        Closes connection with specific client.
        :param client: client's socket object to end communication with.
        """
        client.close()

    def close_socket(self):
        """
        Closes the Tcp server.
        """
        self.__tcp_socket.close()
