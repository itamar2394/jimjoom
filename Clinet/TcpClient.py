import socket
from Errors import DisconnectedServerError, NoInternetError


class TcpClient:

    """
    This class enables Tcp communication with the server. All tcp messages will be directed to port 2394.
    """

    IP_ADDRESS = "127.0.0.1"
    PORT = 2394
    MAX_MSG_SIZE = 1024 * 6
    TIME_OUT_FOR_CONNECTING = 12

    def __init__(self, ip=IP_ADDRESS):
        self.__tcp_socket = None
        self.__ip = ip

    def connect(self):
        """
        Initializes the socket objects and connects it to the server. this will take place before *every* time we want
        to send a request. before connection we'll set the timeout to one second in order to easily find out if there
        is some problem in the server. Ultimately, we would want to leave this timeout like this also for the receiving
        process, but since part of the requests we're sending are waiting requests and those will take time to the
        serer to answer. so after connection we will immediately define the socket as its default state - no timeout at
        all.
        """
        try:
            self.__tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__tcp_socket.settimeout(self.TIME_OUT_FOR_CONNECTING)
            self.__tcp_socket.connect((self.__ip, self.PORT))
        except ConnectionRefusedError:
            raise DisconnectedServerError
        except OSError:
            raise NoInternetError

    def send_request(self, message):
        """
        First connects to the server then sending him a given message.
        :param message: message in string to send.
        """
        self.connect()
        try:
            self.__tcp_socket.send(message.encode())
        except OSError:
            pass

    def receive_response(self):
        """
        Waits for response from the server, the closing the connection.
        :return: the response
        """
        try:
            self.__tcp_socket.settimeout(None)
            response = self.__tcp_socket.recv(TcpClient.MAX_MSG_SIZE)
        except (ConnectionResetError, TimeoutError, socket.timeout):
            raise DisconnectedServerError
        except ConnectionAbortedError:
            return
        finally:
            self.close_connection()
        try:
            response = response.decode()
        except UnicodeDecodeError:
            pass
        self.close_connection()
        return response

    def close_connection(self):
        """
        Closing the tcp socket.
        """
        self.__tcp_socket.close()
