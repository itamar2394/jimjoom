class TcpRequests:

    """
    This class enable easy request-response process with the server, according to the format we defined:
    REQUEST_TYPE:a,b,c when a,b,c are parameters. That way we unite every outgoing request and incoming response and
    apply the same procedure for each of them.
    """

    def __init__(self, tcp_client, logger):
        """
        :param tcp_client: TcpClient object which the communication will occur through.
        :param logger: Logger object for documentation of every request-response.
        """
        self.__tcp_client = tcp_client
        self.__logger = logger

    def request(self, request_type, *args):
        """
        The function is called for *every* request we want to ask the server. The function will build the request
        according to the protocol, send it to the server and wait for response. also logs the request and response.
        :param request_type: string with the wanted request type. should be completely caps locked according to the
         server's protocol.
        :param args: unlimited number of parameters that will be attached to the request. must be a string.
        :return: the server's response.
        """
        request = f"{request_type}:{','.join(args)}"
        self.__tcp_client.send_request(request)
        response = self.__tcp_client.receive_response()
        if isinstance(response, str):
            self.__logger.log(f"{request}   >>>   {response}")
        else:
            self.__logger.log(request)
        return response
