import socket


class UdpClient:

    """
    This class enables the Udp communication with the server.
    """

    IP_ADDRESS = "127.0.0.1"
    MAXIMUM_MESSAGE_SIZE = 1024 * 6

    def __init__(self, port=None, ip=IP_ADDRESS):
        """
        :param port: port to send message through.
        udp_socket: the udp client socket object.
        is_sent_flag: we need this since connection made by sending. we won't be able to receive without sending first.
        """
        self.__udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__ip = ip
        self.__port = port
        self.__is_sent_flag = False

    def set_port(self, port):
        self.__port = port

    def receive(self):
        """
        Waits to receive data from the server.
        :return: the received data.
        """
        if self.__is_sent_flag:
            try:
                data, server = self.__udp_socket.recvfrom(self.MAXIMUM_MESSAGE_SIZE*2)
                return data
            except (OSError, ConnectionResetError):
                return None

    def send(self, message):
        """
        Sends a given message to the server.
        :param message: message in bytes to send.
        """
        if self.__udp_socket and self.__port:
            try:
                self.__udp_socket.sendto(message, (self.__ip, self.__port))
                self.__is_sent_flag = True
            except OSError:
                pass

    def close_connection(self):
        """
        Close the socket. also restart the class attributes to enable another use of the instance in new jam.
        """
        self.__udp_socket.close()
        self.__init__()
