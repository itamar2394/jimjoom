from threading import Thread
from tkinter import ttk, Label, Button, LabelFrame, Tk
import sys
import requests

"""
This file includes errors classes (inheritance from Exceptions) which represent problematic and unexpected situations.
The classes also supply the tools to represent the error to the user in the screen and to try and solve the problem, or
at least to check when its over and recover from it.
"""


class NoInternetError(Exception):

    """
    This class represents a situation where the local device does not have an internet connection.
    """

    @staticmethod
    def screen(root):
        def close_window():
            root.quit()
            root.destroy()
        root.config(bg="#fdde6c")
        no_internet_label = Label(root, text="No internet connection.\nConnect to the network and try again...",
                                  font=("Comic Sans MS", 14), padx=10, pady=10, bg="#fdde6c")
        try_again_button = Button(root, text="try again", font=("Comic Sans MS", 14), command=close_window,
                                  bg="#fdde6c")
        close_button = Button(root, text="close", font=("Comic Sans MS", 14), command=sys.exit, bg="#fdde6c")
        no_internet_label.grid(column=0, row=0, columnspan=2)
        try_again_button.grid(column=0, row=1, pady=10)
        close_button.grid(column=1, row=1, pady=10)
        root.eval('tk::PlaceWindow . center')  # locate the window in the center
        root.overrideredirect(1)  # disable closing the window
        root.mainloop()

    def wait_until_internet_is_back(self, root=None):
        """
        First, the function will hide the current window. then, the function will present the new screen and recheck the
        internet connection every time the user will click on the try again button. If the internet is back we'll
        display the original hidden screen again. If not, we'll call the screen method again, with new Tk object.
        :param root:
        :return:
        """
        if root:
            root.withdraw()
        while True:
            is_the_internet_back = self.check_internet_connection()
            if is_the_internet_back:
                if root:
                    root.deiconify()
                    root.state('zoomed')
                break
            else:
                self.screen(Tk())

    @staticmethod
    def check_internet_connection():
        """
        The function checks if the computer is connected to the internet, using HTTP request to Google.com.
        :return: boolean value whether the device is connected to the internet.
        """
        try:
            requests.get('https://www.google.com')
            return True
        except requests.exceptions.ConnectionError:
            return False


class DisconnectedServerError(Exception):

    """
    This class represents a situation when the server is not active. We'll have to cope with two different types of
    unavailability - server had an critic error, then we'll assume that all his volatile data was deleted, or he just
    wasn't connected to the internet for some time.
    """

    def __init__(self, window=None, tcp_requests=None, on_server_recovery=None, on_server_reset=None, user=None):
        self.__on_server_recovery = on_server_recovery
        self.__on_server_reset = on_server_reset
        self.__user = user
        self.__tcp_requests = tcp_requests
        self.__window = window
        if self.__window:
            self.__root = window.get_frame()
        self.__error_frame = None

    def screen(self):
        self.__root.config(bg="#fdde6c")
        self.__error_frame = LabelFrame(self.__root, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        error_label = Label(self.__error_frame, text="Some error occurred on our server...", font=("Comic Sans MS", 36),
                            fg="black", bg="#fdde6c")
        progress_bar = ttk.Progressbar(self.__error_frame, orient="horizontal", length=300, mode="indeterminate")
        progress_bar.start(10)
        error_label.pack(pady=(self.__window.get_screen_height() // 2, 0), padx=200)
        progress_bar.pack(pady=(30, self.__window.get_screen_height() // 2))
        self.__error_frame.pack()
        self.__error_frame.place(relx=.5, rely=.5, anchor="c")
        reconnect_thread = Thread(target=self.try_to_reconnect)
        reconnect_thread.start()

    def try_to_reconnect(self):
        """
        Constantly tries to reconnect the server using PING requests. When response we received we'll know he is back
        online. We'll try and log in again to the system, with the username and hashed password stored in the User
        object. If we got positive response it is a sign that all the server's data was deleted. In that case, we'll
        return the user to the on_server_reset screen, which should be the menu screen (we already reconnected the user
        hence he doesn't have to pass the connection screens all over again). If, however, we got response that saying
        we already connected to the user, we'll understand that the server's data was saved. Then, we'll call the
        on_server_recovery function.
        """
        while True:
            try:
                self.__tcp_requests.request("PING")
                break
            except DisconnectedServerError:
                pass
        if self.__user:
            try:
                log_in_response = self.__tcp_requests.request("LOG_IN", self.__user.get_username(),
                                                              self.__user.get_hashed_password())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self.__window)
                log_in_response = self.__tcp_requests.request("LOG_IN", self.__user.get_username(),
                                                              self.__user.get_hashed_password())
            log_in_response, info = log_in_response.split(":")
            if info == "ALREADY_IN_USE":
                self.__on_server_recovery()
            elif log_in_response == "TRUE":
                if self.__on_server_reset:
                    self.__on_server_reset(self.__user)
            self.__error_frame.destroy()
        elif self.__on_server_recovery:
            self.__on_server_recovery()
