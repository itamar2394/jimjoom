import requests
import json
import geocoder
import socket
import Errors

"""
This file includes tools to help and manage the matters relating to the user's location. as this module runs, it will 
access the Flagedia api in order to get conversion table between countries names and countries and their internet 
extensions.
"""

API_REQUEST_GET_FLAG = "https://flagcdn.com/36x27/COUNTRY_CODE.png"
API_REQUEST_GET_COUNTRIES_LIST = "https://flagcdn.com/en/codes.json"
try:
    COUNTRIES_NAMES_BY_CODES = json.loads(requests.get(API_REQUEST_GET_COUNTRIES_LIST).text)
    COUNTRIES_CODES_BY_NAMES = {name: code for code, name in COUNTRIES_NAMES_BY_CODES.items()}
    LIST_OF_COUNTRIES = list(COUNTRIES_NAMES_BY_CODES.values())
except requests.exceptions.ConnectionError:
    raise Errors.NoInternetError


def get_flag(country_name):
    """
    Using the Flagedia api to get data image of the flag of a given country. first replace the country name with its
    internet extension using the COUNTRIES_CODES_BY_NAMES dict we created, in order to fit the request to the api
    protocol.
    :param country_name: name of the wanted country's flag.
    :return: image data in bytes.
    """
    image_data = requests.get(API_REQUEST_GET_FLAG.replace("COUNTRY_CODE", COUNTRIES_CODES_BY_NAMES[country_name]),
                              stream=True)
    return image_data.raw


def get_current_location():
    """
    Using the geocoder package to find the client location. since the country information is given in countries codes,
    we'll use the COUNTRIES_NAMES_BY_CODES dict we created in order to return the user's country name.
    :return: name of the country of the user.
    """
    try:
        country_code = geocoder.ip('me').geojson["features"][0]["properties"]["country"].lower()
    except (socket.gaierror, IndexError):
        return None
    return COUNTRIES_NAMES_BY_CODES[country_code]
