from threading import Thread


class StreamManager:

    def __init__(self, speaker_stream, mic_stream, udp_client):
        """
        :param speaker_stream: SpeakerStream object to play the audio through.
        :param mic_stream: MicStream object to receive microphone audio from.
        :param udp_client: UdpClient object to receive and send audio data through.
        """
        self.__speaker_stream = speaker_stream
        self.__mic_stream = mic_stream
        self.__udp_client = udp_client
        self.__is_streaming = False

    def start_stream(self, port):
        """
        Initializing the Udp socket with a given port and then starting two threads. one for listening to the microphone
        and sending to the server and one for receiving audio from the server and playing it through the speaker.
        :param port: port number to communicate on.
        """
        self.__udp_client.set_port(port)
        self.__is_streaming = True
        send_stream = Thread(target=self.send_stream)
        receive_stream = Thread(target=self.receive_stream)
        send_stream.start()
        receive_stream.start()

    def receive_stream(self):
        """
        As long as the streaming process is active we will receive data from the server and add it to the speaker
        stream's buffer.
        :return:
        """
        while self.__is_streaming:
            self.__speaker_stream.add_to_buffer(self.__udp_client.receive())

    def send_stream(self):
        """
        Call mic_stream to start the stream. we will pass the udp_client.send function as parameter in order that every
        input from the microphone will be sent to the server.
        """
        self.__mic_stream.start_stream(self.__udp_client.send)

    def stop_stream(self):
        """
        Stops udp communication with the server, microphone action and speaker action.
        """
        self.__is_streaming = False  # stops the receiving and playing
        self.__mic_stream.stop_stream()  # stops the listening and sending
        self.__udp_client.close_connection()
