from Gui.GuiManager import GuiManager
from TcpClient import TcpClient
from TcpRequests import TcpRequests
from SpeakerStream import SpeakerStream
from MicStream import MicStream
from StreamsManager import StreamManager
from UdpClient import UdpClient
from Logger import Logger
import ipaddress


def main():
    """
    The main function of the client. Asks the user to input the server's ip, initialize necessary objects and calls the
    first screen method of the Gui - splash screen.
    """
    while True:
        with open("IP_config.txt", "r") as f:
            server_ip_address = f.read()
        try:
            ipaddress.ip_address(server_ip_address)
            break
        except ValueError:
            input("Invalid ip address. Press enter to try again.")
    tcp_client = TcpClient(ip=server_ip_address)
    logger = Logger("JimJoom client logging").initialize("%(asctime)s     %(message)s", "jimjoom client logging.log")
    tcp_requests = TcpRequests(tcp_client, logger)
    speaker_stream = SpeakerStream()
    mic_stream = MicStream()
    udp_client = UdpClient(ip=server_ip_address)
    streams_manager = StreamManager(speaker_stream, mic_stream, udp_client)
    gui_manager = GuiManager(tcp_requests, streams_manager)
    gui_manager.initialize_screen_settings()
    gui_manager.splash_screen()


if __name__ == '__main__':
    main()
