from Gui.Window import Window
from Errors import DisconnectedServerError, NoInternetError
from tkinter import Label, LabelFrame, Button, DISABLED, messagebox
from PIL import ImageTk, Image
import pyperclip
from threading import Thread


class WaitingForInvitedUsersToJoin(Window):
    def __init__(self, frame, previous_window, next_window, tcp_requests, user, jam):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = user
        self.__jam = jam
        self.__users_state = {}
        self.__start_jam_button = None

    def open(self):
        wait_for_invited_users_thread = Thread(target=self.wait_for_invited_users_to_join)
        wait_for_invited_users_thread.start()

        jam_id_frame = LabelFrame(self._root, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        jam_id_frame.pack()
        this_is_your_code_label = Label(jam_id_frame, text="This is your jam code:", fg="black", bg="#fdde6c",
                                        font=("Comic Sans MS", 22))
        jam_id_label = Label(jam_id_frame, text=self.__jam.jam_id(), bg="#0a3042", fg="white", borderwidth=4,
                             padx=10, pady=10)
        copy_id_to_clipboard_button = Button(jam_id_frame, text="Copy me!", fg="black", font=("Comic Sans MS", 12),
                                             command=lambda: pyperclip.copy(self.__jam.jam_id()), bg="#fdde6c")
        this_is_your_code_label.pack(pady=(50, 0))
        jam_id_label.pack(pady=(30, 15))
        copy_id_to_clipboard_button.pack(pady=(0, 15))

        waiting_for_users_to_connect_label = Label(self._root, font=("Comic Sans MS", 18), fg="black",
                                                   text="Wait for at least one user to connect"
                                                        " before starting the jam.",  bg="#fdde6c")
        waiting_for_users_to_connect_label.pack(padx=10)
        users_state_frame = LabelFrame(self._root, borderwidth=6, bg="#fdde6c")
        users_state_frame.pack(padx=20, pady=10)

        for username in self.__jam.get_participants_user_names():
            if username != self.__user.get_username():
                user_frame = LabelFrame(users_state_frame, borderwidth=0, highlightthickness=0, bg="#fdde6c")
                user_frame.pack(padx=15, pady=15)
                user_label = Label(user_frame, text=username, font=("Comic Sans MS", 18), fg="black", bg="#fdde6c")
                state_icon_image = ImageTk.PhotoImage(Image.open("Icons/offline.png"))
                state_icon_label = Label(user_frame, image=state_icon_image, bg="#fdde6c")
                self.__users_state[username] = state_icon_label
                state_icon_label.image = state_icon_image
                user_label.grid(column=0, row=0, padx=(10, 15))
                state_icon_label.grid(column=1, row=0, padx=(15, 10))

        self.__start_jam_button = Button(self._root, command=self.start_jam, text="Start the jam",
                                         font=("Comic Sans MS", 24), bg="#fdde6c", state=DISABLED)
        cancel_jam_button = Button(self._root, command=self.cancel_jam, text="Cancel jam",
                                   font=("Comic Sans MS", 16), bg="#fdde6c")
        cancel_jam_button.pack(pady=10)
        self.__start_jam_button.pack(pady=15)

    def wait_for_invited_users_to_join(self):
        """
        As long as we do not get a message that the jam started or canceled, we'll repeatedly send the server waiting
        request so he would update us when some invited user had joined left. When such update we'll be received we'll
        change the icon beside the appropriate username to offline/online accordingly.
        """
        connected_users_counter = 0
        while True:
            try:
                try:
                    update = self.__tcp_requests.request("WAIT_FOR_INVITED_USERS", self.__jam.jam_id())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    update = self.__tcp_requests.request("WAIT_FOR_INVITED_USERS", self.__jam.jam_id())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                        on_server_reset=self._previous_window, user=self.__user,
                                        on_server_recovery=self.wait_for_invited_users_to_join).screen()
                return
            update, username = update.split(":")
            if update == "JAM_STARTED" or update == "JAM_CANCELED":
                break
            elif update == "CONNECT":
                connected_users_counter += 1
                state_icon_image = ImageTk.PhotoImage(Image.open("Icons/online.png"))
                self.__start_jam_button["state"] = "normal"
            else:  # => update equals "DISCONNECT"
                connected_users_counter -= 1
                state_icon_image = ImageTk.PhotoImage(Image.open("Icons/offline.png"))
                if connected_users_counter == 0:
                    self.__start_jam_button["state"] = "disable"
            self.__users_state[username].configure(image=state_icon_image)
            self.__users_state[username].image = state_icon_image

    def cancel_jam(self):
        """
        Sends message to the server that we are canceling the jam.
        """
        answer = messagebox.askyesno(title="Cancel jam", message="Do you wish to proceed?")
        if not answer:
            return False
        try:
            try:
                self.__tcp_requests.request("CANCEL_JAM", self.__jam.jam_id(), self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("CANCEL_JAM", self.__jam.jam_id(), self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_reset=self._previous_window,
                                    on_server_recovery=self.cancel_jam).screen()
            return
        self._previous_window(self.__user)
        return True

    def start_jam(self):
        """
        Informs the server that we're starting the jam. We'll add to our Jam object the connected users list we got in
        response and move to the next screen.
        """
        try:
            try:
                start_jam_response = self.__tcp_requests.request("START_JAM", self.__jam.jam_id())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                start_jam_response = self.__tcp_requests.request("START_JAM", self.__jam.jam_id())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.start_jam).screen()
            return
        self.__jam.set_connected_users(start_jam_response.split(","))
        self._next_window(self.__user, self.__jam)

    def on_close(self):
        """
        after making sure the user wants to close the program, we'll update the server that we're cancelling the jam
        and closing JimJoom.
        """
        whether_to_close = self.cancel_jam()
        if whether_to_close:
            try:
                try:
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_recovery=self.on_close).screen()
            return True
        return False

    def __str__(self):
        return "Waiting for invited users to Join"
