from Gui.Window import Window
from Errors import DisconnectedServerError, NoInternetError
from tkinter import LabelFrame, Label, Button, messagebox
from threading import Thread
from LocationsHelper import get_flag
from PIL import Image, ImageTk


class JamScreen(Window):

    MAX_MINUTES_OF_RECORDING = 10

    def __init__(self, frame, previous_window, next_window, tcp_requests, user, jam, streams_manager):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = user
        self.__jam = jam
        self.__streams_manager = streams_manager
        self.__instrument_icon_labels = {}
        self.__is_recording = False
        self.__length_of_recording = 0
        self.__jam_frame = None
        self.__record_button = None
        self.__timer = None

    def open(self):
        self.__user.set_is_in_jam(True)
        self.__jam_frame = LabelFrame(self._root, padx=10, pady=10, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        self.__jam_frame.pack()
        users_frame = LabelFrame(self.__jam_frame, bg="#fdde6c", borderwidth=0, highlightthickness=0)
        users_frame.pack()
        counter = 0
        locations = [user_data["location"] for user_data in self.__jam.get_participants_info().values()]
        repeated_locations = [location for location in locations if locations.count(location) > 1]
        repeated_flags = {location: ImageTk.PhotoImage(Image.open(get_flag(location))) for location in
                          repeated_locations}
        for username, user_data in self.__jam.get_participants_info().items():
            user_frame = LabelFrame(users_frame, padx=10, pady=10, bg="#fdde6c", borderwidth=0, highlightthickness=0)
            user_frame.grid(column=counter % 2, row=counter // 2)
            name_and_country_frame = LabelFrame(user_frame, borderwidth=0, highlightthickness=0, bg="#fdde6c")
            name_and_country_frame.pack()
            user_label = Label(name_and_country_frame, text=username, font=("Comic Sans MS", 18), bg="#fdde6c")
            location = user_data["location"]
            if location not in repeated_flags.keys():
                country_icon = ImageTk.PhotoImage(Image.open(get_flag(location)))
            else:
                country_icon = repeated_flags[location]
            country_label = Label(name_and_country_frame, image=country_icon, bg="#fdde6c")
            country_label.image = country_icon
            instrument_icon = ImageTk.PhotoImage(Image.open(f"Icons/{user_data['instrument']}.png"))
            if username not in self.__jam.get_connected_users():
                instrument_icon = ImageTk.PhotoImage(Image.open("Icons/disconnected.png"))
            instrument_label = Label(user_frame, image=instrument_icon, bg="#fdde6c")
            instrument_label.image = instrument_icon
            self.__instrument_icon_labels[username] = instrument_label
            user_label.grid(row=0, column=0)
            country_label.grid(row=0, column=1)
            instrument_label.pack()
            counter += 1
        bottom_bar_frame = LabelFrame(self.__jam_frame, pady=10, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        leave_button = Button(bottom_bar_frame, text="Leave jam", font=("Comic Sans MS", 18), padx=10,
                              command=self.leave_jam, bg="#fdde6c")
        record_image = ImageTk.PhotoImage(Image.open(f"Icons/record.png"))
        self.__record_button = Button(bottom_bar_frame, image=record_image, command=self.record, padx=10, bg="#fdde6c")
        self.__record_button.image = record_image
        bottom_bar_frame.pack(padx=10, pady=(30, 10))
        leave_button.grid(column=0, row=0, padx=10, ipady=10)
        self.__record_button.grid(column=1, row=0, padx=10, ipadx=10, ipady=10)
        wait_for_jam_updates = Thread(target=self.wait_for_jam_updates)
        wait_for_jam_updates.start()
        self.__streams_manager.start_stream(self.__user.get_port())

    def wait_for_jam_updates(self):
        """
        As long as the user is present in the jam, we'll ask the server to update us when some change occurs in our jam,
        i.e., someone left or joined. When someone joins, we'll change the icon below his username to an image of  his
        instrument instead of offline icon. When someone left' we'll do the opposite change. Then we'll also check if
        the new number of connected users in this jam is not one (that would be us). If it is, we'll leave the jam
        ourselves.
        """
        while self.__user.is_in_jam():
            try:
                try:
                    update = self.__tcp_requests.request("WAIT_FOR_JAM_UPDATES", self.__user.get_username(),
                                                         self.__jam.jam_id())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    update = self.__tcp_requests.request("WAIT_FOR_JAM_UPDATES", self.__user.get_username(),
                                                         self.__jam.jam_id())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                        on_server_reset=self._previous_window, user=self.__user,
                                        on_server_recovery=self.wait_for_jam_updates).screen()
                return
            message_type, username = update.split(":")
            if message_type != "YOU_LEFT":
                if message_type == "SOMEONE_LEFT":
                    self.__jam.remove_from_connected_users(username)
                    if len(self.__jam.get_connected_users()) == 1:
                        messagebox.showinfo(title="Jam is over", message="The last jam member left. Jam is over.")
                        self.leave_jam(forced=True)
                        return
                    icon_image = ImageTk.PhotoImage(Image.open(f"Icons/disconnected.png"))
                else:  # => message_type = "SOMEONE_JOINED"
                    self.__jam.add_to_connected_users(username)
                    instrument = self.__jam.get_participants_info()[username]["instrument"]
                    icon_image = ImageTk.PhotoImage(Image.open(f"Icons/{instrument}.png"))
                self.__instrument_icon_labels[username].config(image=icon_image)
                self.__instrument_icon_labels[username].image = icon_image

    def update_timer(self):
        """
        This function will increase the timer in one second. If we are still recording the function will wait one second
        and then call itself. If the timer reached the MAX_MINUTES_OF_RECORDING limit, we'll stop the recording.
        """
        time = self.__timer.cget("text")
        minutes, seconds = time.split(":")
        minutes = int(minutes)
        seconds = int(seconds)
        if seconds == 59:
            if minutes == self.MAX_MINUTES_OF_RECORDING - 1:
                self.stop_recording(disable=True)
            minutes = str(int(minutes) + 1)
            seconds = "00"
        else:
            seconds = seconds + 1
            if seconds < 10:
                seconds = f"0{str(seconds)}"
        updated_time = f"0{minutes}:{str(seconds)}"
        self.__timer.config(text=updated_time)
        if self.__is_recording:
            self.__timer.after(1000, self.update_timer)  # time is in milliseconds, updates every one second.

    def record(self):
        """
        Asks the server to start recording, changes the record button to stop recording button and starts/continues the
        timer.
        """
        try:
            try:
                self.__tcp_requests.request("START_RECORDING", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("START_RECORDING", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_reset=self._previous_window,
                                    on_server_recovery=self.record).screen()
            return
        self.__is_recording = True
        stop_recording_image = ImageTk.PhotoImage(Image.open(f"Icons/stop recording.png"))
        self.__record_button.configure(image=stop_recording_image, command=self.stop_recording)
        self.__record_button.image = stop_recording_image
        timer_frame = LabelFrame(self.__jam_frame, bg="#fdde6c")
        if not self.__timer:
            self.__timer = Label(timer_frame, text="00:00", font=("Comic Sans MS", 16), bg="#fdde6c")
            time_counter_label = Label(timer_frame, text="note: you can record 10 minutes at most.",
                                       font=("Comic Sans MS", 10), bg="#fdde6c")
            time_counter_label.pack()
            self.__timer.pack()
        timer_frame.pack()
        self.update_timer()

    def stop_recording(self, disable=False):
        """
        Asks the server to stop recording, changes the stop recording button to record button and stops the timer by
        setting the is recording flag to False.
        :param disable: Whether to disable the record button (when reached recording length limit).
        """
        self.__is_recording = False
        record_image = ImageTk.PhotoImage(Image.open(f"Icons/record.png"))
        self.__record_button.configure(image=record_image, command=self.record)
        self.__record_button.image = record_image
        try:
            try:
                self.__tcp_requests.request("STOP_RECORDING", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("STOP_RECORDING", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.stop_recording).screen()
            return
        if disable:
            self.__record_button["state"] = "disabled"

    def leave_jam(self, forced=False, close_program=False):
        """
        If the user wasn't forced to leave the jam we'll ask him if he's sure. if he does, we'll inform the sever we're
        leaving the jam and stop the streaming. If, as response from the server, we got number, this number indicates
        the size of the recording and we'll move to the next screen. If this is just a confirmation, we hadn't recorded
        and we'll go back to the previous window.
        :param forced: whether the user was forced to leave the jam (everyone left except him).
        :param close_program: whether was called from the on_close method.
        :return boolean value whether to close.
        """
        whether_to_delete_recording = False
        if not forced:
            if close_program and self.__timer:  # meaning: has recorded and pressed close
                are_you_sure = messagebox.askyesno(title="close JimJoom", message="Do you wish to proceed? "
                                                                                  "your recording won't be saved!")
                if not are_you_sure:
                    return False
                whether_to_delete_recording = True  # not deleting here since we have to leave jam first
            elif close_program:
                are_you_sure = messagebox.askyesno(title="clsoe JimJoom", message="Do you wish to proceed?")
                if not are_you_sure:
                    return False
            else:
                are_you_sure = messagebox.askyesno(title="leave jam", message="Do you wish to proceed?")
                if not are_you_sure:
                    return False
        self.__user.set_is_in_jam(False)
        self.__streams_manager.stop_stream()
        try:
            try:
                length_of_recording = self.__tcp_requests.request("LEAVE_JAM", self.__user.get_username(),
                                                                  self.__jam.jam_id())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                length_of_recording = self.__tcp_requests.request("LEAVE_JAM", self.__user.get_username(),
                                                                  self.__jam.jam_id())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_reset=self._previous_window,
                                    on_server_recovery=self.leave_jam).screen()
            return
        if not close_program:
            if length_of_recording.isdigit():  # meaning => has recorded
                self._next_window(self.__user, int(length_of_recording))
            else:
                self._previous_window(self.__user)
        else:
            if whether_to_delete_recording:
                try:
                    try:
                        self.__tcp_requests.request("DELETE_RECORDING", self.__user.get_username())
                    except NoInternetError:
                        NoInternetError().wait_until_internet_is_back(root=self._root)
                        self.__tcp_requests.request("DELETE_RECORDING", self.__user.get_username())
                except DisconnectedServerError:
                    DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                            on_server_reset=self._previous_window,
                                            on_server_recovery=self.leave_jam).screen()
                    return
            return True

    def on_close(self):
        """
        Calls the leave jam function. If the user did not regret, we will also send update to the server, saying we also
        disconnecting completely.
        """
        whether_to_close = self.leave_jam(close_program=True)
        if whether_to_close:
            try:
                try:
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_recovery=self.on_close).screen()
            return True
        return False

    def __str__(self):
        return "Jam"
