from Gui.InitialScreen import InitialScreen
from Gui.Login import Login
from Gui.SignUp import SignUp
from Gui.Menu import Menu
from Gui.CreatePrivateJam import CreatePrivateJam
from Gui.JoinPrivateJam import JoinPrivateJam
from Gui.JoinRandomJam import JoinRandomJam
from Gui.WaitingForHostToStart import WaitingForHostToStart
from Gui.WaitingForInvitedUsersToJoin import WaitingForInvitedUsersToJoin
from Gui.JamScreen import JamScreen
from Gui.ReceiveRecording import ReceiveRecording
from Gui.SplashScreen import SplashScreen
from tkinter import Tk
import sys


def change_screen(define_screen):
    def actual_decorator(*args, **kwargs):
        """
        This function will warp every screen method. it will first call the warped function, which will define the
        current_window attribute as new object of the matching screen class. then it will clear the screen and call the
        open method of the new Window object.
        :param args: args of the warped screen method.
        :param kwargs: key args of the warped screen method.
        """
        define_screen(*args, **kwargs)
        gui_manager = args[0]  # since first argument is always self (decorates only GuiManager methods).
        new_window = gui_manager.get_current_window()
        new_window.clear_screen()
        new_window.open()
    return actual_decorator


class GuiManager:

    """
    This class enables the graphic user interface. It has a 'screen method' for each screen in the program. Every screen
    method, using 'change screen' as decorator, will clear the screen, define a new one, and open it.
    Each one of the screen methods creating an instance of the appropriate screen class. There is one parameter that
    will always be passed to the screen classes - root, the Tk object. Usually, the class would also get link for the
    next and previous screen functions. Most of the screen classes will also receive the TcpRequests object to enable
    sending requests to the server. The screens that take place after the user connects to the system, will also get a
    User object. those who takes place after connection to the jam, will receive in addition to the User object, a Jam
    object.
    """

    def __init__(self, tcp_requests, streams_manager):
        """
        :param tcp_requests: TcpRequests object to pass to the screen classes who needs to contact the server.
        :param streams_manager: StreamsManager object to pass the jam screen class.
        root: Tk object of the window.
        current_window: Window object (or child object) which is currently presented to the user.
        """
        self.__root = Tk()
        self.__current_window = None
        self.__tcp_requests = tcp_requests
        self.__streams_manager = streams_manager

    def close_window(self):
        """
        This will occur every time the user will try to click the X button. We will run the on_close method of the
        current screen and just if we got True value we'll destroy the window. A value is returned since sometimes, when
        the user is in the middle of some process (waiting for host to start the jam for example), we'll want to ask him
        if he's sure he wants to close the program, and just if he's indeed sure of course, then we'll close it.
        :return:
        """
        whether_to_close = self.__current_window.on_close()
        if whether_to_close:
            self.__root.destroy()
            sys.exit()

    def initialize_screen_settings(self):
        """
        Defines settings for the Tk object: title, default font and combobox font, background color, close function. we
        also sets the screen as unchangeable, size wise.
        """
        self.__root.title('JimJoom')
        self.__root.resizable(False, False)  # disable resizing the window
        self.__root.protocol("WM_DELETE_WINDOW", self.close_window)
        self.__root.option_add("*TCombobox*Listbox*Font", ("Comic Sans MS", 14))
        self.__root.config(bg="black")
        self.__root.option_add("*Font", ("Comic Sans MS", 36))

    def get_current_window(self):
        return self.__current_window

    def set_current_window(self, window):
        self.__current_window = window

    @change_screen
    def splash_screen(self):
        self.__current_window = SplashScreen(self.__root, self.initial_screen)

    @change_screen
    def initial_screen(self):
        self.__current_window = InitialScreen(self.__root, {"log_in": self.login, "sign_up": self.sign_up})

    @change_screen
    def login(self):
        self.__current_window = Login(self.__root, self.initial_screen, self.menu, self.__tcp_requests)

    @change_screen
    def sign_up(self):
        self.__current_window = SignUp(self.__root, self.initial_screen, self.menu, self.__tcp_requests)

    @change_screen
    def menu(self, user):
        self.__current_window = Menu(self.__root, {"join_random_jam": self.join_random_jam,
                                                   "create_private_jam": self.create_private_jam,
                                                   "join_private_jam": self.join_private_jam}, self.__tcp_requests,
                                     user)

    @change_screen
    def join_random_jam(self, user):
        self.__current_window = JoinRandomJam(self.__root, self.menu, self.jam, self.__tcp_requests, user)

    @change_screen
    def create_private_jam(self, user):
        self.__current_window = CreatePrivateJam(self.__root, self.menu, self.waiting_for_invited_users_to_join,
                                                 self.__tcp_requests, user)

    @change_screen
    def join_private_jam(self, user):
        self.__current_window = JoinPrivateJam(self.__root, self.menu,
                                               {"waiting_for_host_to_start": self.waiting_for_host_to_start,
                                                "jam": self.jam}, self.__tcp_requests, user)

    @change_screen
    def waiting_for_host_to_start(self, user, jam):
        self.__current_window = WaitingForHostToStart(self.__root, self.menu, self.jam, self.__tcp_requests, user, jam)

    @change_screen
    def waiting_for_invited_users_to_join(self, user, jam):
        self.__current_window = WaitingForInvitedUsersToJoin(self.__root, self.menu, self.jam, self.__tcp_requests,
                                                             user, jam)

    @change_screen
    def jam(self, user, jam):
        self.__current_window = JamScreen(self.__root, self.menu, self.receive_recording, self.__tcp_requests, user,
                                          jam, self.__streams_manager)

    @change_screen
    def receive_recording(self, user, length_of_recording):
        self.__current_window = ReceiveRecording(self.__root, self.menu, self.__tcp_requests, user, length_of_recording)
