from Gui.Window import Window
from tkinter import Button, CENTER, Label, LabelFrame, ttk, messagebox
from threading import Thread
from Errors import DisconnectedServerError, NoInternetError


class WaitingForHostToStart(Window):
    def __init__(self, frame, previous_window, next_window, tcp_requests, user, jam):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = user
        self.__jam = jam

    def open(self):
        wait_for_host_to_start_thread = Thread(target=self.wait_for_host_to_start)
        wait_for_host_to_start_thread.start()
        waiting_frame = LabelFrame(self._root, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        waiting_for_the_host_label = Label(waiting_frame, text=f"Waiting for the host ({self.__jam.host()})"
                                                               f" to start this jam...", bg="#fdde6c",
                                           font=("Comic Sans MS", 26))
        progress_bar = ttk.Progressbar(waiting_frame, orient="horizontal", length=300, mode="indeterminate")
        progress_bar.start(5)
        waiting_frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        waiting_for_the_host_label.pack(pady=10)
        progress_bar.pack(pady=10)
        disconnect_button = Button(waiting_frame, command=self.disconnect_from_jam_that_did_not_open_yet,
                                   text="Disconnect", font=("Comic Sans MS", 22), bg="#fdde6c")
        disconnect_button.pack(pady=10)

    def wait_for_host_to_start(self):
        """
        We'll send a waiting response to the server, asking him to update us when the jam is starting. when we'll get
        this message We'll add to our Jam object the connected users list that was attached and move to the next screen.
        If we received a message saying the jam was cancelled we'll go to previous window.
        """
        try:
            try:
                update = self.__tcp_requests.request("WAIT_FOR_HOST", self.__jam.jam_id(), self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                update = self.__tcp_requests.request("WAIT_FOR_HOST", self.__jam.jam_id(), self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.wait_for_host_to_start).screen()
            return
        if update.startswith("JAM_STARTED"):
            message, data_about_jam = update.split(":")
            self.__jam.set_connected_users(data_about_jam.split(","))
            self._next_window(self.__user, self.__jam)
        elif update == "JAM_WAS_CLOSED_BY_THE_HOST":
            messagebox.showinfo(title="Jam was closed", message="Jam was closed by the host...")
            self._previous_window(self.__user)
        # if none of the three above is True the message was "YOU_DISCONNECTED_THE_JAM". We have to do nothing in this
        # case.

    def disconnect_from_jam_that_did_not_open_yet(self):
        """
        After asking the user if he's sure, we'll update the host we're leaving the jam and move back to the previous
        window.
        """
        are_you_sure = messagebox.askyesno(title="Leave jam", message="Do you wish to proceed?")
        if not are_you_sure:
            return False
        try:
            try:
                self.__tcp_requests.request("DISCONNECT_FORM_JAM_THAT_DID_NOT_START_YET",
                                            self.__user.get_username(), self.__jam.jam_id())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("DISCONNECT_FORM_JAM_THAT_DID_NOT_START_YET",
                                            self.__user.get_username(), self.__jam.jam_id())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.disconnect_from_jam_that_did_not_open_yet).screen()
        self._previous_window(self.__user)
        return True

    def on_close(self):
        """
        We'll call the disconnect_from_jam_that_did_not_open_yet method, which will also make sure the user wants to
        disconnect jam. if he does we'll inform the server also about general disconnection.
        """
        whether_to_close = self.disconnect_from_jam_that_did_not_open_yet()
        if whether_to_close:
            try:
                try:
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_recovery=self.on_close).screen()
            return True
        return False

    def __str__(self):
        return "Waiting for host to start"
