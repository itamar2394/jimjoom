from Gui.Window import Window
from tkinter import ttk, Label, LabelFrame, StringVar, Button
from Errors import DisconnectedServerError, NoInternetError
from ValidityProtocols import check_if_user_names_are_valid
from Jam import Jam
from User import User


class CreatePrivateJam(Window):

    MAX_NUMBER_OF_PARTICIPANTS_IN_JAM = 4

    def __init__(self, frame, previous_window, next_window, tcp_requests, user):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__user = user
        self.__tcp_requests = tcp_requests
        self.__error_label = None
        self.__user_invite_entries = []
        self.__valid_user_names_that_does_not_exist = []

    def open(self):
        self._root.option_add("*TCombobox*Listbox*Font", ("Comic Sans MS", 14))
        self.top_bar(self.__user)
        instructions_label = Label(self._root, font=("Comic Sans MS", 16),
                                   text="Invite up to 3 users to your jam.\nJust add their user names down below:",
                                   padx=10, pady=10, bg="#fdde6c")
        instructions_label.pack()
        users_invite_frame = LabelFrame(self._root, padx=10, pady=10, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        users_invite_frame.pack()
        for _ in range(self.MAX_NUMBER_OF_PARTICIPANTS_IN_JAM - 1):
            user_invite_entry = StringVar()
            user_invite_combobox = ttk.Combobox(users_invite_frame, textvariable=user_invite_entry,
                                                values=self.__user.get_friends(), width=22)
            self.__user_invite_entries.append(user_invite_entry)
            user_invite_combobox.pack()

        submit_button = Button(text="Create jam", bg="#0a3042", fg="white",
                               command=lambda: self.check_if_new_jam_is_valid())
        submit_button.pack(padx=20, pady=20)

    def check_if_new_jam_is_valid(self):
        """
        Checks if all entered user names are possible. If they does we'll ask the server about every one if he's exists.
        If they all exists, we'll request the server to create the jam. we'll create a Jam object with the data we
        received in response and move to the waiting for invited users screen. If we'll see the user input is invalid
        we'll display appropriate error.
        """
        self.__error_label = self.clear_error_label(self.__error_label)
        user_names = [entry.get() for entry in self.__user_invite_entries if entry.get()]
        if list(filter(lambda x: x in self.__valid_user_names_that_does_not_exist, user_names)):
            self.__error_label["text"] = "One or more entered user names does not exist. try again."
            self.__error_label.pack()
            return
        is_valid = check_if_user_names_are_valid(user_names, self.__user.get_username())
        if is_valid == "Valid":
            for username in user_names:
                try:
                    try:
                        response = self.__tcp_requests.request("CHECK_IF_USERNAME_EXISTS", username)
                    except NoInternetError:
                        NoInternetError().wait_until_internet_is_back(root=self._root)
                        response = self.__tcp_requests.request("CHECK_IF_USERNAME_EXISTS", username)
                except DisconnectedServerError:
                    DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                            on_server_reset=self._previous_window, user=self.__user,
                                            on_server_recovery=self.check_if_new_jam_is_valid).screen()
                    return
                if response == "FALSE":
                    self.__valid_user_names_that_does_not_exist.append(username)
                    self.__error_label["text"] = "One or more entered user names does not exist. try again."
                    self.__error_label.pack()
                    return
        else:
            self.__error_label["text"] = is_valid
            self.__error_label.pack()
            return
        user_names.append(self.__user.get_username())
        try:
            try:
                data_about_jam = self.__tcp_requests.request("CREATE_JAM", ",".join(user_names))
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                data_about_jam = self.__tcp_requests.request("CREATE_JAM", ",".join(user_names))
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.check_if_new_jam_is_valid).screen()
            return
        jam_id, port, users_info = data_about_jam.split(":")
        self.__user.set_port(int(port))
        users_info = users_info.split("+")
        jam_members = []
        for user_info in users_info:
            username, instrument, location = user_info.split("-")
            jam_members.append(User(username=username, instrument=instrument, location=location))
        jam = Jam(jam_id, jam_members, [self.__user.get_username()], self.__user.get_username())
        self._next_window(self.__user, jam)

    def on_close(self):
        """
        Informs the server about the closure.
        """
        try:
            try:
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_recovery=self.on_close).screen()
        return True

    def __str__(self):
        return "Create Private Jam"
