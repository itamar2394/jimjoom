from Gui.Window import Window
from User import User
from Errors import DisconnectedServerError, NoInternetError
from tkinter import Entry, Button, Label, Canvas, PhotoImage, messagebox
from ValidityProtocols import check_if_username_and_password_are_valid
import hashlib


class Login(Window):
    def __init__(self, frame, previous_window, next_window, tcp_requests):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = User()
        self.__username_entry = None
        self.__password_entry = None
        self.__error_label = None
        self.__hashed_password = None

    def open(self):
        self._root.config(bg="#fdde6c")
        user_icon_canvas = Canvas(self._root, bg='#fdde6c', bd=0, highlightthickness=0, width=180, height=180)
        user_icon_image = PhotoImage(file="Icons/user.png")
        user_icon_canvas.create_image(90, 90, image=user_icon_image)

        username_label = Label(self._root, text="User name:", bg="#0a3042", fg="white",
                               font=("Comic Sans MS", 30), width=20)
        password_label = Label(self._root, text="Password:", bg="#0a3042", fg="white",
                               font=("Comic Sans MS", 30), width=20)
        self.__username_entry = Entry(self._root, font=("Comic Sans MS", 40), bg="#0a3042", fg="#fdde6c",
                                      insertbackground="white", width=15)
        self.__password_entry = Entry(self._root, font=("Comic Sans MS", 40), show="*", bg="#0a3042",
                                      fg="#fdde6c", width=15, insertbackground="white")
        submit_button = Button(self._root, text="Log in", bg="#0a3042", fg="white", command=self.log_in)

        self.top_bar()
        user_icon_canvas.pack(pady=20)
        username_label.pack()
        self.__username_entry.pack()
        password_label.pack(pady=(10, 0))
        self.__password_entry.pack()
        submit_button.pack(pady=10)
        self._root.mainloop()

    def get_log_in_response(self, username, password):
        """
        Gets username and password and returns an error or data about user.
        """
        error = check_if_username_and_password_are_valid(username, password)
        if error == "Valid":
            self.__hashed_password = hashlib.sha224(password.encode()).hexdigest()
            try:
                try:
                    log_in_response = self.__tcp_requests.request("LOG_IN", username, self.__hashed_password)
                    return log_in_response
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    log_in_response = self.__tcp_requests.request("LOG_IN", username, self.__hashed_password)
                    return log_in_response
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, on_server_reset=self.log_in,
                                        on_server_recovery=self.log_in).screen()
        return error

    def log_in(self):
        """
        analyzing the log in response. if its an error, displaying it on the screen. if its positive response, creating
        an User object and moving to next screen.
        :return:
        """
        self.__error_label = self.clear_error_label(self.__error_label)
        log_in_response = self.get_log_in_response(self.__username_entry.get(), self.__password_entry.get())
        if log_in_response.count(":"):
            log_in_response, info = log_in_response.split(":")
            if info == "ALREADY_IN_USE":
                error = "You are already connected in other device."
            elif log_in_response == "TRUE":
                friends = info.split("+")
                friends = None if friends == [""] else friends
                self.__user.set_username(self.__username_entry.get())
                self.__user.set_hashed_password(self.__hashed_password)
                self.__user.set_friends(friends)
                messagebox.showinfo(title="Logged in successfully", message=f"Welcome back "
                                                                            f"{self.__user.get_username()}!")
                self._next_window(self.__user)
                return
            elif log_in_response == "FALSE":
                error = "Wrong username or password."
            else:
                error = info
        else:
            error = log_in_response
        self.__error_label["text"] = error
        self.__error_label.pack()

    def on_close(self):
        return True

    def __str__(self):
        return "Log in"
