from abc import ABC, abstractmethod
from tkinter import LabelFrame, Button, Label
from PIL import Image, ImageTk


class Window(ABC):

    """
    This is abstract class representing a window.
    """

    def __init__(self, frame, next_window=None, previous_window=None):
        self._root = frame
        self._next_window = next_window
        self._previous_window = previous_window

    def get_frame(self):
        return self._root

    def get_screen_width(self):
        return self._root.winfo_screenwidth()

    def get_screen_height(self):
        return self._root.winfo_screenheight()

    def clear_screen(self):
        """
        This function clears the screen from all its content.
        """
        for widget in self._root.winfo_children():
            widget.destroy()

    def clear_error_label(self, error_label):
        """
        This function gets error label and deletes it from the screen.
        :param error_label: tkinter.Label object
        :return: new label.
        """
        if error_label:
            error_label.pack_forget()
        return Label(self._root, fg="red", font=("Comic Sans MS", 16), bg="#fdde6c")

    def top_bar(self, *args):
        """
        This function will create a top bar for the current window, including title with its name and a go back arrow
        which will take the user to the _previous_window.
        :param args: this args are intended to the previous window function.
        """
        top_bar_frame = LabelFrame(self._root, bg="#0a3042")
        go_back_icon = ImageTk.PhotoImage(Image.open("Icons/arrow.png"))
        go_back_button = Button(top_bar_frame, image=go_back_icon, command=lambda: (self._previous_window(*args)),
                                bg="#0a3042", bd=0)
        go_back_button.image = go_back_icon
        headline_label = Label(top_bar_frame, text=str(self), font=("Comic Sans MS", 36), bg="#0a3042", fg="white")
        go_back_button.grid(column=0, row=0, padx=(20, 100), pady=10)
        headline_label.grid(column=1, row=0, padx=(0, 80))
        top_bar_frame.pack()

    def adjust_image_size(self, image, percentage):
        """
        This function is used to fit image in the screen, in a way it won't run over the screen's limits. It takes only
        the height of the screen into account, that's according to the assumption that this is the problematic axis. the
        function will resize a given image in a way that its original axis' ratio will be saved, but the height of the
        image will be in some given percentage of the screen height.
        :param image: PIL.Image object.
        :param percentage: the wanted percentage from the screen's height.
        :return: resized PIL.Image object.
        """
        current_width, current_height = image.size
        new_height = self.get_screen_height() * percentage
        if new_height > current_height:
            new_height = current_height
        new_width = new_height / current_height * current_width
        if new_width > current_width:
            new_width = current_width
        resized_image = image.resize((int(new_width), int(new_height)), Image.ANTIALIAS)
        return resized_image

    @abstractmethod
    def on_close(self):
        """
        This function will take place each time before the user tries to close the window.
        :return: boolean value whether to actually close it.
        """

    @abstractmethod
    def open(self):
        """
        In this function the screen will display itself on the window.
        """
        pass

    @abstractmethod
    def __str__(self):
        """
        In this function the screen will return its name. It is used in the top_bar method, where we will define the
        title of the screen as its string value.
        """
        pass
