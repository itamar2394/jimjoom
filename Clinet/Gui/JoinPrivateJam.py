from Gui.Window import Window
from tkinter import Label, Entry, Button
from Errors import DisconnectedServerError, NoInternetError
from Jam import Jam
from User import User


class JoinPrivateJam(Window):

    JAM_CODE_LENGTH = 4

    def __init__(self, frame, previous_window, next_window, tcp_requests, user):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__user = user
        self.__tcp_requests = tcp_requests
        self.__error_label = None

    def open(self):
        self.top_bar(self.__user)

        enter_code_label = Label(text="Enter the jam code:", fg="black", bg="#fdde6c")
        code_label_entry = Entry(self._root, bg="#0a3042", fg="white",
                                 insertbackground="white", width=6, justify='center')
        join_jam_button = Button(text="Join jam", bg="#0a3042", fg="white",
                                 command=lambda: self.is_valid_jam_code(code_label_entry.get()))
        enter_code_label.pack(padx=15)
        code_label_entry.pack(pady=10)
        join_jam_button.pack(pady=20)

    def is_valid_jam_code(self, jam_code):
        """
        Checks if the jam code entered by the user is possible (4 digits). if it does we'll send a request to join that
        jam. we'll analyze the response - if the request has been approved we'll create a Jam object. If the jam is
        already active we'll move to the jam screen, if not we'll move the the waiting for the host screen. If we
        received an error we'll display it on the screen.
        """
        self.__error_label = self.clear_error_label(self.__error_label)
        if len(jam_code) == self.JAM_CODE_LENGTH and all(char.isdigit() for char in jam_code):
            try:
                try:
                    data_about_jam = self.__tcp_requests.request("JOIN_PRIVATE_JAM", self.__user.get_username(),
                                                                 jam_code)
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    data_about_jam = self.__tcp_requests.request("JOIN_PRIVATE_JAM", self.__user.get_username(),
                                                                 jam_code)
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_reset=self._previous_window,
                                        on_server_recovery=self.is_valid_jam_code).screen()
                return
            am_i_in_jam, port, data_about_jam, connected_users = data_about_jam.split(":")
            if am_i_in_jam != "FALSE":
                jam_id = port[:-1]
                self.__user.set_port(int(port))
                users_info = data_about_jam.split("+")
                host = ""
                jam_members = []
                for user_info in users_info:
                    username, instrument, location = user_info.split("-")
                    jam_members.append(User(username=username, location=location, instrument=instrument))
                    host = username  # host appears last
                connected_users = connected_users.split(",")
                jam = Jam(jam_id, jam_members, connected_users, host)
                if am_i_in_jam == "TRUE":
                    self._next_window["waiting_for_host_to_start"](self.__user, jam)
                    return
                # meaning: jam is already active
                self._next_window["jam"](self.__user, jam)
                return
        self.__error_label["text"] = "invalid code"
        self.__error_label.pack()

    def on_close(self):
        """
        Informs server about the closure.
        """
        try:
            try:
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_recovery=self.on_close).screen()
        return True

    def __str__(self):
        return "Join Private Jam"
