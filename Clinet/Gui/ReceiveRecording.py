from Gui.Window import Window
from Errors import DisconnectedServerError, NoInternetError
from contextlib import closing
from threading import Thread
from tkinter import Label, ttk, LabelFrame, CENTER, messagebox
from datetime import datetime
import wave
import os


class ReceiveRecording(Window):

    SAMPLE_WIDTH = 2
    NUMBER_OF_CHANNELS = 1
    SAMPLE_RATE = 44100
    CHUNK = 1024 * 6
    TIME_FORMAT = "%m-%d-%Y %H-%M-%S"

    def __init__(self, frame, next_window, tcp_requests, user, length_of_recording):
        super().__init__(frame, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = user
        self.__length_of_recording = length_of_recording
        self.__progress_bar = None

    def open(self):
        receive_recording_frame = LabelFrame(self._root, bg="#fdde6c", borderwidth=0, highlightthickness=0)
        receive_recording_frame.pack()
        receive_recording_frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        loading_label = Label(receive_recording_frame, text="Downloading your recording...",
                              font=(f"Comic Sans MS", 18), bg="#fdde6c")
        self.__progress_bar = ttk.Progressbar(receive_recording_frame, orient="horizontal", length=300,
                                              mode="determinate")
        loading_label.pack(pady=10)
        self.__progress_bar.pack(pady=10, padx=20)

        downloading_thread = Thread(target=self.receive_recording)
        downloading_thread.start()

    def save_data(self, file_name, audio_bytes):
        path_to_recording_file = f"{file_name}.wav"
        if os.path.exists(path_to_recording_file):
            with closing(wave.open(path_to_recording_file, 'rb')) as current_recording:
                length = current_recording.getnframes()
                current_data = current_recording.readframes(length)
            audio_bytes = current_data + audio_bytes
        with closing(wave.open(path_to_recording_file, "wb")) as recording_file:
            recording_file.setnchannels(self.NUMBER_OF_CHANNELS)
            recording_file.setsampwidth(self.SAMPLE_WIDTH)
            recording_file.setframerate(self.SAMPLE_RATE)
            recording_file.writeframesraw(audio_bytes)

    def receive_recording(self):
        number_of_segments = self.__length_of_recording // self.CHUNK
        if self.__length_of_recording % self.CHUNK != 0:
            number_of_segments += 1
        now = datetime.now()
        date_time = now.strftime(self.TIME_FORMAT + f" {self.__user.get_username()}")
        for sequence_number in range(number_of_segments):
            is_last_flag = str(sequence_number == number_of_segments - 1)
            try:
                try:
                    data = self.__tcp_requests.request("RECEIVE_RECORDING", self.__user.get_username(),
                                                       str(sequence_number), is_last_flag)
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    data = self.__tcp_requests.request("RECEIVE_RECORDING", self.__user.get_username(),
                                                       str(sequence_number), is_last_flag)
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                        on_server_reset=self._next_window, user=self.__user,
                                        on_server_recovery=self.receive_recording).screen()
                return
            self.save_data(date_time, data)
            recording_progression = sequence_number / number_of_segments * 100
            self.__progress_bar["value"] = recording_progression
        self._next_window(self.__user)

    def on_close(self):
        whether_to_close = messagebox.askyesno(title="Close JimJoom", message="Do you wish to proceed?\nYour recording "
                                                                              "might not be completely downloaded!")
        if whether_to_close:
            try:
                try:
                    self.__tcp_requests.request("DELETE_RECORDING", self.__user.get_username())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    self.__tcp_requests.request("DELETE_RECORDING", self.__user.get_username())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_recovery=self.on_close).screen()
                return
            try:
                try:
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                        on_server_recovery=self.on_close).screen()
            return True
        return False

    def __str__(self):
        return "Receive Recording"
