from Gui.Window import Window
from PIL import Image, ImageTk
from threading import Thread
from playsound import playsound, PlaysoundException
from tkinter import Label


class SplashScreen(Window):

    MUSICAL_INTRO_FILE = "AudioFiles/GuitarIntro.mp3"
    INTRO_LENGTH_IN_MS = 4500

    def __init__(self, frame, next_window):
        super().__init__(frame, next_window=next_window)

    def play_intro(self):
        try:
            playsound(self.MUSICAL_INTRO_FILE)
        except PlaysoundException:
            pass

    def open(self):
        self._root.after(self.INTRO_LENGTH_IN_MS, self._next_window)
        play_audio_thread = Thread(target=self.play_intro)
        play_audio_thread.start()
        self._root.overrideredirect(True)
        logo_image = Image.open(f"Icons/JimJoom_logo_splash_screen.jpg")
        image_width, image_height = logo_image.size
        logo_image = ImageTk.PhotoImage(logo_image)
        north_west_starting_point = {"x": self.get_screen_width() // 2 - image_width // 2,
                                     "y": self.get_screen_height() // 2 - image_height // 2}
        self._root.geometry(f"{image_width}x{image_height}+{north_west_starting_point['x']}+"
                            f"{north_west_starting_point['y']}")
        logo_image_label = Label(self._root, image=logo_image)
        logo_image_label.pack()
        self._root.mainloop()  # since this is the first screen

    def __str__(self):
        return "Splash Screen"

    def on_close(self):
        return
