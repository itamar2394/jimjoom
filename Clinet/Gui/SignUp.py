from Gui.Window import Window
from User import User
from ChecklistCombobox import ChecklistCombobox
from Errors import DisconnectedServerError, NoInternetError
from ValidityProtocols import check_sign_up_values
from tkinter import ttk, StringVar, Label, Entry, Button, messagebox
import hashlib
try:
    from LocationsHelper import *
except NoInternetError:
    NoInternetError().wait_until_internet_is_back()
    from LocationsHelper import *


class SignUp(Window):

    LIST_OF_INSTRUMENTS = ['Accordion', 'Acoustic guitar', 'Banjo', 'Bass guitar', 'Cello', 'Clarinet', 'Drums',
                           'Electric guitar', 'Flute', 'Harmonica', 'Harp', 'Mandolin', 'Piano', 'Saxophone', 'Singer',
                           'Sitar', 'Synthesizer', 'Trombone', 'Trumpet', 'Ukulele', 'Violin']
    LIST_OF_GENRES = ['Blues', 'Classical', 'Country', 'Electronic', 'Folk', 'Funk', "Indie", 'Jazz', 'Metal', 'Pop',
                      'Punk', 'R&B', 'Rock']

    def __init__(self, frame, previous_window, next_window, tcp_requests):
        super().__init__(frame, previous_window=previous_window, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__location = StringVar()
        self.__user = User()
        self.__confirm_password_entry = None
        self.__username_entry = None
        self.__password_entry = None
        self.__confirm_password = None
        self.__instrument_combobox = None
        self.__genres_checklist_combobox = None
        self.__error_label = None
        self.__hashed_password = None

    def open(self):
        self._root.config(bg="#fdde6c")
        self._root.option_add("*TCombobox*Listbox*Font", ("Comic Sans MS", 14))
        sign_up_font = ("Comic Sans MS", 25)
        username_label = Label(self._root, text="User name:", bg="#0a3042", fg="white",
                               font=sign_up_font, width=25)
        password_label = Label(self._root, text="Password:", bg="#0a3042", fg="white",
                               font=sign_up_font, width=25)
        confirm_password_label = Label(self._root, text="Confirm password:", bg="#0a3042", fg="white",
                                       font=sign_up_font, width=25)
        location_label = Label(self._root, text="Location:", bg="#0a3042", fg="white",
                               font=sign_up_font, width=25)
        instrument_label = Label(self._root, text="Instrument:", bg="#0a3042", fg="white",
                                 font=sign_up_font, width=25)
        genres_label = Label(self._root, text="Genres:", bg="#0a3042", fg="white",
                             font=sign_up_font, width=25)
        self.__username_entry = Entry(self._root, font=sign_up_font,
                                      insertbackground="white", bg="#0a3042", fg="#fdde6c", width=25)
        self.__password_entry = Entry(self._root, show="*", font=sign_up_font,
                                      insertbackground="white", bg="#0a3042", fg="#fdde6c", width=25)
        self.__confirm_password_entry = Entry(self._root, show="*", insertbackground="white",
                                              font=sign_up_font, bg="#0a3042",
                                              fg="#fdde6c", width=25)
        location_combobox = ttk.Combobox(self._root, textvariable=self.__location,
                                         values=LIST_OF_COUNTRIES, width=36, font=("Comic Sans MS", 15),
                                         state="readonly")
        current_location = get_current_location()
        if current_location:
            location_combobox.current(LIST_OF_COUNTRIES.index(current_location))
        current_instrument = StringVar()
        self.__instrument_combobox = ttk.Combobox(self._root, textvariable=current_instrument, state="readonly",
                                                  values=self.LIST_OF_INSTRUMENTS, width=36,
                                                  font=("Comic Sans MS", 15))
        self.__genres_checklist_combobox = ChecklistCombobox(self._root, values=self.LIST_OF_GENRES, width=36,
                                                             state="readonly", font=("Comic Sans MS", 15))
        submit_button = Button(self._root, text="Sign up", font=("Comic Sans MS", 24), bg="#0a3042", fg="white",
                               command=self.sign_up)

        self.top_bar()
        username_label.pack(pady=(20, 0))
        self.__username_entry.pack()
        password_label.pack(pady=(5, 0))
        self.__password_entry.pack()
        confirm_password_label.pack(pady=(5, 0))
        self.__confirm_password_entry.pack()
        location_label.pack(pady=(5, 0))
        location_combobox.pack()
        instrument_label.pack(pady=(5, 0))
        self.__instrument_combobox.pack()
        genres_label.pack(pady=(5, 0))
        self.__genres_checklist_combobox.pack()
        submit_button.pack(pady=(10, 10))

    def get_sign_up_response(self, username, password, confirm_password, location, instrument, genres):
        """
        Gets all values the user entered and returns an error or confirmation to the creation from the server.
        """
        error = check_sign_up_values(username, password, confirm_password, location, instrument, genres)
        if error == "Valid":
            self.__hashed_password = hashlib.sha224(password.encode()).hexdigest()
            if isinstance(genres, list):
                genres = "+".join(genres)
            try:
                try:
                    sign_up_response = self.__tcp_requests.request("SIGN_UP", username, self.__hashed_password,
                                                                   location, instrument, genres)
                except NoInternetError:
                    NoInternetError().wait_until_internet_is_back(root=self._root)
                    sign_up_response = self.__tcp_requests.request("SIGN_UP", username, self.__hashed_password,
                                                                   location, instrument, genres)
                    return sign_up_response
            except DisconnectedServerError:
                DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, on_server_reset=self.sign_up,
                                        on_server_recovery=self.sign_up).screen()
                return
            return sign_up_response
        return error

    def sign_up(self):
        """
        Analyzing the log in response. If its an error, we'll display it to the user. If its a positive response we'll
        create a User object and move the next screen.
        """
        self.__error_label = self.clear_error_label(self.__error_label)
        sign_up_response = self.get_sign_up_response(self.__username_entry.get(), self.__password_entry.get(),
                                                     self.__confirm_password_entry.get(), self.__location.get(),
                                                     self.__instrument_combobox.get(),
                                                     self.__genres_checklist_combobox.get())
        if sign_up_response == "DONE_SUCCESSFULLY":
            self.__user.set_hashed_password(self.__hashed_password)
            self.__user.set_username(self.__username_entry.get())
            messagebox.showinfo(title="Registered successfully", message=f"Welcome {self.__user.get_username()}!")
            self._next_window(self.__user)
            return
        elif sign_up_response == "USERNAME_ALREADY_EXISTS":
            error = "This username is already in use."
        else:
            error = sign_up_response
        self.__error_label["text"] = error
        self.__error_label.pack()

    def on_close(self):
        return True

    def __str__(self):
        return "Sign Up"
