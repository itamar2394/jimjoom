from Gui.Window import Window
from tkinter import PhotoImage, Label, LabelFrame, Button, LEFT, RIGHT
from PIL import Image, ImageTk


class InitialScreen(Window):

    def __init__(self, frame, next_window):
        super().__init__(frame, next_window=next_window)

    def open(self):
        self._root.overrideredirect(False)
        self._root.state('zoomed')  # set window size as full screen
        self._root.iconbitmap('Icons/logo.ico')
        self._root.config(bg="black")
        virtual_pixel = PhotoImage(width=1, height=1)
        logo_image = Image.open("Icons/JimJoom_logo.jpg")
        resized_logo_image = self.adjust_image_size(logo_image, 0.8)
        image_width, image_height = resized_logo_image.size
        resized_logo_image = ImageTk.PhotoImage(resized_logo_image)
        logo_image_label = Label(self._root, image=resized_logo_image, borderwidth=0)
        logo_image_label.image = resized_logo_image
        buttons_frame = LabelFrame(self._root, borderwidth=0, highlightthickness=0)
        buttons_height = self.get_screen_height() - image_height
        log_in_button = Button(buttons_frame, text="Log in", command=self._next_window["log_in"], bg="#0a3042",
                               image=virtual_pixel, fg="white", width=(image_width // 2) * 0.98,
                               height=buttons_height, compound="c", borderwidth=4)
        sign_up_button = Button(buttons_frame, text="Sign up", command=self._next_window["sign_up"],
                                image=virtual_pixel,
                                bg="#0a3042", fg="white", width=(image_width // 2) * 0.98,
                                height=buttons_height, compound="c", borderwidth=4)
        sign_up_button.image = virtual_pixel
        log_in_button.image = virtual_pixel

        logo_image_label.pack()
        buttons_frame.pack()
        log_in_button.pack(side=LEFT)
        sign_up_button.pack(side=RIGHT)

    def on_close(self):
        return True

    def __str__(self):
        return "Initial Screen"
