from Gui.Window import Window
from Errors import DisconnectedServerError, NoInternetError
from Jam import Jam
from User import User
from tkinter import ttk, LabelFrame, Label, Button, CENTER, messagebox
from threading import Thread


class JoinRandomJam(Window):
    def __init__(self, frame, previous_window, next_window, tcp_requests, user):
        super().__init__(frame, previous_window=previous_window,
                         next_window=next_window)
        self.__user = user
        self.__tcp_requests = tcp_requests

    def open(self):
        wait_for_random_jam_thread = Thread(target=self.wait_for_random_jam)
        wait_for_random_jam_thread.start()

        loading_frame = LabelFrame(self._root, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        loading_label = Label(loading_frame, text="Waiting for available jam...", fg="black", bg="#fdde6c")
        progress_bar = ttk.Progressbar(loading_frame, orient="horizontal", length=300, mode="indeterminate")
        cancel_button = Button(loading_frame, text="cancel", command=self.stop_waiting_for_random_jam,
                               bg="#0a3042", fg="white")

        loading_frame.pack()
        loading_frame.place(relx=0.5, rely=0.5, anchor=CENTER)
        loading_label.pack(padx=10)
        progress_bar.pack(pady=30)
        cancel_button.pack(pady=30)
        progress_bar.start(5)

    def wait_for_random_jam(self):
        """
        Sends waiting request to the server, so he would let us know when a random jam is available. then we'll create
        a Jam object with the attached data we got and move to the next screen.
        """
        try:
            try:
                data_about_jam = self.__tcp_requests.request("ADD_TO_RANDOM_JAM_WAITING_LIST",
                                                             self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                data_about_jam = self.__tcp_requests.request("ADD_TO_RANDOM_JAM_WAITING_LIST",
                                                             self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.wait_for_random_jam).screen()
            return
        if data_about_jam != "YOU_HAVE_LEFT_THE_RANDOM_JAM_WAITING_LIST":
            port, users_info = data_about_jam.split(":")
            self.__user.set_port(int(port))
            jam_id = str(port)[:-1]
            jam_members = []
            for user_info in users_info.split("+"):
                username, instrument, location = user_info.split("-")
                jam_members.append(User(username=username, location=location, instrument=instrument))
            connected_users = [user.get_username() for user in jam_members]
            jam = Jam(jam_id, jam_members, connected_users)
            self._next_window(self.__user, jam)

    def stop_waiting_for_random_jam(self, closing_program=False):
        """
        If the user is sure, we will ask the server to get us out of the random jam waiting list, then going back to the
        previous window.
        """
        if not closing_program:
            if not messagebox.askyesno(title="Stop waiting for random jam", message="Do you wish to proceed?"):
                return
        try:
            try:
                self.__tcp_requests.request("STOP_WAITING_FOR_RANDOM_JAM", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("STOP_WAITING_FOR_RANDOM_JAM", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_reset=self._previous_window, user=self.__user,
                                    on_server_recovery=self.stop_waiting_for_random_jam).screen()
            return
        self._previous_window(self.__user)

    def on_close(self):
        """
        First we'll pop up a question box asking the server if he's sure he wants to close the program. If he does,
        we'll inform the server that we're stop waiting for random jam and then that we are closing the program.
        """
        are_you_sure = messagebox.askyesno(title="close JimJoom", message="Do you wish to proceed?")
        if not are_you_sure:
            return False
        self.stop_waiting_for_random_jam(closing_program=True)
        try:
            try:
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests, user=self.__user,
                                    on_server_recovery=self.on_close).screen()
        return True

    def __str__(self):
        return "Join random screen"
