from Gui.Window import Window
from tkinter import LabelFrame, Button
from Errors import DisconnectedServerError, NoInternetError


class Menu(Window):
    def __init__(self, frame, next_window, tcp_requests, user):
        super().__init__(frame=frame, next_window=next_window)
        self.__tcp_requests = tcp_requests
        self.__user = user

    def open(self):
        join_jam_frame = LabelFrame(self._root, padx=10, pady=10, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        random_jam_button = Button(join_jam_frame, text="Join a random jam", height=2, width=20,
                                   fg="white", command=lambda: (self._next_window["join_random_jam"](self.__user)),
                                   bg="#0a3042")
        existing_jam_button = Button(join_jam_frame, text="Join a private jam", height=2, width=20,
                                     fg="white", command=lambda: (self._next_window["join_private_jam"](self.__user)),
                                     bg="#0a3042")

        random_jam_button.pack(padx=2, pady=5)
        create_jam_frame = LabelFrame(self._root, padx=10, pady=10, borderwidth=0, highlightthickness=0, bg="#fdde6c")
        create_jam_button = Button(create_jam_frame, text="Create private jam", height=2, width=20, fg="white",
                                   command=lambda: (self._next_window["create_private_jam"](self.__user)), bg="#0a3042")

        join_jam_frame.pack(padx=20, pady=20)
        existing_jam_button.pack(padx=2, pady=5)
        create_jam_frame.pack(padx=20, pady=10)
        create_jam_button.pack(padx=2, pady=5)

    def on_close(self):
        """
        since we already connected to some account, we'll have to inform the server about the closure..
        :return:
        """
        try:
            try:
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
            except NoInternetError:
                NoInternetError().wait_until_internet_is_back(root=self._root)
                self.__tcp_requests.request("CLOSE_CLIENT", self.__user.get_username())
        except DisconnectedServerError:
            DisconnectedServerError(window=self, tcp_requests=self.__tcp_requests,
                                    on_server_recovery=self.on_close).screen()
        return True

    def __str__(self):
        return "Menu"
