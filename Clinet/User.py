class User:

    """
    This class representing a user. It is created for our user, i.e the user who's connected to the program and for each
    other user who participate in our user's jams. With the help of this class we can store and access users data
    easily.
    """

    def __init__(self, username=None, hashed_password=None, location=None, instrument=None, friends=()):
        """
        :param username: the user's username.
        :param location: name of the country the user came from.
        :param instrument: name of the instrument the user is playing on.
        :param friends: list of the user names of the friends of the user.
        port: private port for udp communication. initialized as None since not in jam when created.
        """
        self.__username = username
        self.__hashed_password = hashed_password
        self.__friends = friends
        self.__location = location
        self.__instrument = instrument
        self.__port = None
        self.__is_in_jam = False

    def get_port(self):
        return self.__port

    def get_username(self):
        return self.__username

    def get_instrument(self):
        return self.__instrument

    def get_location(self):
        return self.__location

    def get_friends(self):
        return self.__friends

    def get_hashed_password(self):
        return self.__hashed_password

    def set_hashed_password(self, value):
        self.__hashed_password = value

    def set_port(self, value):
        self.__port = value

    def set_username(self, value):
        self.__username = value

    def set_friends(self, value):
        self.__friends = value

    def set_is_in_jam(self, value):
        self.__is_in_jam = value

    def is_in_jam(self):
        return self.__is_in_jam

    def __str__(self):
        return str(self.__username)
