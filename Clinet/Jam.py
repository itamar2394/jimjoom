class Jam:

    """
    This class representing a group of User objects. It is created for each jam our user participates in.
    """
    def __init__(self, jam_id, participants, connected_users, host_username=None):
        """
        :param jam_id: the numeric identifier of the jam.
        :param participants: list with the User objects of the users in the jam.
        :param connected_users: list of strings with the user names of the currently connected users.
        :param host_username: string with the username of the host. potential argument, since random jams does not have
        a host.
        """
        self.__jam_id = jam_id
        self.__participants = participants
        self.__connected_users = connected_users
        self.__host_username = host_username

    def jam_id(self):
        return self.__jam_id

    def get_connected_users(self):
        return self.__connected_users

    def host(self):
        return self.__host_username

    def set_connected_users(self, connected_users):
        self.__connected_users = connected_users

    def remove_from_connected_users(self, username):
        self.__connected_users.remove(username)

    def add_to_connected_users(self, username):
        self.__connected_users.append(username)

    def get_participants_user_names(self):
        return [user.get_username() for user in self.__participants]

    def get_participants_info(self):
        """
        This function used to get the required info about the jams' users, for the jam screen.
        :return: dictionary with sub dictionary for each username in the jam, containing his instrument and location.
        """
        return {user.get_username(): {"location": user.get_location(), "instrument": user.get_instrument()}
                for user in self}

    def __iter__(self):
        return iter(self.__participants)
