from pyaudio import PyAudio, paInt16


class SpeakerStream:

    """
    This class enables to play a audio bytes through the speaker.
    """

    FORMAT = paInt16  # => sample width = 2
    CHANNELS = 1
    RATE = 44100

    def __init__(self):
        """
        stream: PyAudio object which we define as output.
        """
        self.__stream = PyAudio().open(format=self.FORMAT, channels=self.CHANNELS, rate=self.RATE, output=True)

    def add_to_buffer(self, data):
        """
        Writes given audio data to the stream object, i.e playing the through the speaker.
        :param data: byte string with audio data.
        """
        if data:
            try:
                self.__stream.write(data)
            except OSError:
                pass
