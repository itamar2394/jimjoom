MIN_LENGTH_OF_PASSWORD_AND_USERNAME = 8
MAX_LENGTH_OF_PASSWORD_AND_USERNAME = 14
INVALID_CHARS = ",+-:"

"""
This file is a collection of functions which checks the validity of input from the user. They returns appropriate error 
to show the user or positive response.
"""


def check_password_or_username(password_or_username):
    """
    This functions checks whether some password or username is valid according to the protocol we defined - its length
    is between MIN_LENGTH_OF_PASSWORD_AND_USERNAME and MAX_LENGTH_OF_PASSWORD_AND_USERNAME and does not including any of
    the INVALID_CHARS. this function is in used only inside the file.
    :param password_or_username: string with potential password or username.
    :return: boolean value whether this username or password adheres the our protocol.
    """
    return not (len(password_or_username) < MIN_LENGTH_OF_PASSWORD_AND_USERNAME or
                len(password_or_username) > MAX_LENGTH_OF_PASSWORD_AND_USERNAME or
                any([char in password_or_username for char in INVALID_CHARS]))


def check_if_user_names_are_valid(user_names, my_username):
    """
    This function is used when the user is trying to create private jam. It will check if the user names he entered are
    even possible, this in order to avoid unnecessary requests to the server. the function will check whether there are
    invited users at all, whether the user did not invite himself, whether there isn't a username that was invited more
    than twice, and whether all user names are rational ones.
    :param user_names: list of the invited user names.
    :param my_username: the inviter username.
    :return: If one of criteria detailed above is false, we will return appropriate message. If all are true we'll
    return "Valid".
    """
    if not any(user_names):
        return "At least one user must be invited to your jam."
    if max(map(lambda current_username: user_names.count(current_username), user_names)) > 1:
        return "You cannot invite the same user twice."
    if my_username in user_names:
        return "You cannot invite yourself to your jam." \
               " Don't worry, you'll be there for sure."
    if not all(list(map(check_password_or_username, user_names))):
        return "One or more entered user names does not exist. try again."
    return "Valid"


def check_if_username_and_password_are_valid(username, password):
    """
    This function will be called when the user is trying to log in. It will check if the values he entered are possible
    at all. It will check whether all fields are filled with something and then that they both adheres our protocol.
    :return: appropriate error. "Valid" if possible.
    """
    if not (password and username):
        return "Some fields are missing."
    if not all(list(map(check_password_or_username, [username, password]))):
        return "Wrong user name or password. Try again."
    return "Valid"


def check_sign_up_values(username, password, confirm_password, location, instrument, genres):
    """
    This function is called when the user is trying to create a new account. We'll check if the values he entered are
    valid, i.e, all fields are filled, password is the same as the password confirmation, password and username does not
    include INVALID_CHARS, longer that MIN_LENGTH_OF_PASSWORD_AND_USERNAME, and shorter than
    MAX_LENGTH_OF_PASSWORD_AND_USERNAME. For the last three we could have used the check_password_or_username function,
    but we want specific error, so the user will not have to guess where is the problem.
    """
    if not (password and username and location and instrument and genres):
        return "Some fields are missing."
    if password != confirm_password:
        return "Passwords does not match."
    if any(char in password + username for char in INVALID_CHARS):
        return "Username or password can not contain the following characters +-,:"
    if len(username) < MIN_LENGTH_OF_PASSWORD_AND_USERNAME or len(password) < MIN_LENGTH_OF_PASSWORD_AND_USERNAME:
        return "Username and password must be in length of at least 8 characters."
    if len(username) > MAX_LENGTH_OF_PASSWORD_AND_USERNAME or len(password) > MAX_LENGTH_OF_PASSWORD_AND_USERNAME:
        return "Username and password can not be longer than 14 characters."
    return "Valid"
