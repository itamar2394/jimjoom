from pyaudio import PyAudio, paInt16


class MicStream:

    """
    This class enables to receive the data of the coming audio from the microphone.
    """

    FORMAT = paInt16  # => sample width = 2
    CHANNELS = 1
    RATE = 44100
    CHUNK = 1024 * 6

    def __init__(self):
        self.__is_streaming = False

    def start_stream(self, input_data):
        """
        First, the function will defined PyAudio object with input=True. Then, as long as we streaming, we will read
        chunks, in the fixed size we set, from the mic stream and pass it to a given function.
        :param input_data: a function to which the audio data will be passed.
        """
        audio = PyAudio()
        stream = audio.open(format=self.FORMAT, channels=self.CHANNELS, rate=self.RATE, input=True,
                            frames_per_buffer=self.CHUNK)
        self.__is_streaming = True
        while self.__is_streaming:
            try:
                input_data(stream.read(MicStream.CHUNK))
            except OSError:
                pass
        stream.close()
        audio.terminate()

    def stop_stream(self):
        """
        sets the __is_streaming flag to false in order to stop the while loop in the streaming function.
        """
        self.__is_streaming = False
